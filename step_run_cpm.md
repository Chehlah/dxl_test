# launch dynamixel joint mode
sudo chmod a+rw /dev/ttyUSB0
roslaunch dxl_test control_manager.launch
roslaunch dxl_test start_cpm_mode.launch

# run joy
sudo chmod a+rw /dev/input/js0 
>> set joy Logitech to "Xinput"
rosrun joy joy_node

# launch F/T sensor w/ net_box 
>> check IP address connected on browser 
roslaunch netft_rdt_driver ft_sensor.launch 
>> check setting param in launch file 

# run cpm code
rosrun dxl_test cpm_mode.py

# run service 'bias' F/T sensor
rosservice call /ft_sensor/bias_cmd "cmd: 'bias'"

# plot topic
/ft_sensor/netft_data/wrench/force/x
/ft_sensor/netft_data/wrench/force/y

# record topic 
rostopic echo /topic_name -p > file_name.csv
rostopic echo /force_raw_product -p > cpm_A79_050719_.csv

>> Name record 050719
1 mg 19 mg_2 17
2 aufa 17 aufa_2 22
3 pat 19
4 fah 20
5 amin 14
6 bank 16
7 ang 17

>> 010519
1 9
2 13
3 16
4 20
5 19
