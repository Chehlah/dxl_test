# run joy
sudo chmod a+rw /dev/input/js0 
>> set joy Logitech to "Xinput"
rosrun joy joy_node

# launch dynamixel wheel mode
sudo chmod a+rw /dev/ttyUSB0
roslaunch dxl_test control_manager.launch
roslaunch dxl_test start_cpm_wheel_mode.launch

# launch F/T sensor w/ net_box 
>> check IP address connected on browser 
roslaunch netft_rdt_driver ft_sensor.launch 
>> check setting param in launch file 
# run service 'bias' F/T sensor
rosservice call /ft_sensor/bias_cmd "cmd: 'bias'"

# run main
rosrun dxl_test cpm_wheel_mode.py
