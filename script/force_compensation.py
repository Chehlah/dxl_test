#!/usr/bin/env python

import csv
import numpy as np
import math as m

import pandas as pd
from sklearn import svm
import pickle

import rospy
from std_msgs.msg import Float64MultiArray
from ros_myo.msg import EmgArray
from std_msgs.msg import Float64

from rospy.core import rospyinfo

class Mechine_Learing():

	read_EMG   = EmgArray()
	stack_EMG  = []
	rms_EMG    = []
	mva_EMG	   = []
	limit      = 100 # window size
	data_train = []
	current_pos = 0.0
	force_predict = Float64()

	def __init__(self):
		rospy.init_node('force_compensation', anonymous=True)
		rospy.Subscriber("/myo_raw/myo_emg", EmgArray , self.cbEMGraw)
		rospy.Subscriber("/current_pos", Float64 , self.cbCurrentPos)
		self.pub_force = rospy.Publisher("/compensation/effort", Float64 , queue_size=10)

		self.rms_EMG = [0.0]*8 
		self.mva_EMG = [0.0]*8
		self.data_train = [0.0]*9

		self.str_ = ("start : limit {}".format(self.limit))
		rospy.loginfo(self.str_)

		# data_x = pd.read_csv('/home/aufa/code Python ML/tun_100/x_train.csv',header=None)
		# data_t = pd.read_csv('/home/aufa/code Python ML/tun_100/t_train.csv',header=None)
		# x_train = data_x.iloc[:,0:9] 
		# t_train = data_t.iloc[:,0]  
		# self.str_ = "Get datasets"
		# rospy.loginfo(self.str_)

		#Load_model >>>>>>>>>>>>>>>>>>>>
		self.str_ = "Load save model ..."
		rospy.loginfo(self.str_)
		filename = '/home/aufa/code Python ML/tun_100/svr_model4_100.sav'
		# pickle.dump(regr, open(filename, 'wb'))
		self.load_regr = pickle.load(open(filename, 'rb'))
		self.str_ = "Done ..."
		rospy.loginfo(self.str_)

		# self.str_ = "Start learn model ..."
		# rospy.loginfo(self.str_)
		# self.regr = svm.SVR(kernel='rbf',gamma=1.15E-02,C=6.37,epsilon=1.00E-06)
		# self.regr.fit(x_train, t_train)
		# self.str_ = "Complete fit model"
		# rospy.loginfo(self.str_)
		
		# self.str_ = "Predict Data"
		# rospy.loginfo(self.str_)
		# self.str_ = "Force = {}".format(self.load_regr.predict([4.1309,6.9268,4.3105,2.8983,2.9715,5.4,3.44,2.2,2.23]))
		# self.str_ = "Score = {}".format(self.regr.score(x_train, t_train))
		# rospy.loginfo(self.str_)
		# self.str_ = "Done ..."
		# rospy.loginfo(self.str_)

	def cbCurrentPos(self,data):
		self.current_pos = data.data # deg

	def cbEMGraw(self,data):
		self.stack_EMG.append(data.data)
		if len(self.stack_EMG) > self.limit:
			self.rms_multical(self.stack_EMG[0:self.limit])
			self.mva_multical(self.stack_EMG[0:self.limit])
			###
			self.data_train[0] = 90 - self.current_pos
			self.data_train[1] = self.rms_EMG[2]
			self.data_train[2] = self.rms_EMG[3]
			self.data_train[3] = self.rms_EMG[4]
			self.data_train[4] = self.rms_EMG[5]
			self.data_train[5] = self.mva_EMG[2]
			self.data_train[6] = self.mva_EMG[3]
			self.data_train[7] = self.mva_EMG[4]
			self.data_train[8] = self.mva_EMG[5]
			self.force_predict.data = self.load_regr.predict(self.data_train)
			self.pub_force.publish(self.force_predict)
			# Filter Force
			
			###
			self.stack_EMG.pop(0)
		else:
			pass

	def rms_multical(self,dataSets):
		stack = []
		for nn in range(0,8):
			stack.append([i[nn] for i in dataSets])
			self.rms_EMG[nn] = np.sqrt(np.mean(np.array(stack[nn])**2))
	
	def mva_multical(self,dataSets):
		stack =[]
		weights = np.ones(self.limit) / self.limit
		for nn in range(0,8):
			stack.append([i[nn] for i in dataSets])
			ans = np.convolve(stack[nn], weights, mode='valid')
			self.mva_EMG[nn] = ans[0]

if __name__ == "__main__":
	try:
		run = Mechine_Learing()
		rospy.spin()
	except rospy.ROSInterruptException:
		pass