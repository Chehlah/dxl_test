#!/usr/bin/env python
'''
	readme.md file name's "step run cpm wheel pid.md"
	Create: 15/10/2021 
'''

import rospy
from ros_myo.msg import EmgArray
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64
import math as m 

class emg_raw():

	current_pos = 0.0
	force_x = Float64()
	data_raw = Float64MultiArray()
	str_ = ""

	def __init__(self):
		rospy.init_node('cpm_emg_rec_raw', anonymous=True)
		# position
		rospy.Subscriber("/current_pos", Float64 , self.cbCurrentPos)
		# force
		rospy.Subscriber("/Fx", Float64 , self.cbForceX)
		# emg
		rospy.Subscriber("/myo_raw/myo_emg", EmgArray , self.cbEMGraw)
		# velocity trick
		# rospy.Subscriber("/v_const", Float64 , self.cbVelocityConstant)
		rospy.Subscriber("/stateVelocity", Float64 , self.cbForRec)
		
		   

		self.pub_data_raw = rospy.Publisher("/rec_raw/emg", Float64MultiArray , queue_size=10)

		self.data_raw.data = [0.0]*10

		self.str_ = "start"
		rospy.loginfo(self.str_)

	def cbForRec(self,data):
		if data.data < -1.0:
			self.pub_data_raw.publish(self.data_raw)
		else:
			pass

	def cbCurrentPos(self,data):
		self.data_raw.data[0] = data.data # deg
		
	def cbEMGraw(self,data):
		self.data_raw.data[1] = data.data[0]
		self.data_raw.data[2] = data.data[1]
		self.data_raw.data[3] = data.data[2]
		self.data_raw.data[4] = data.data[3] 
		self.data_raw.data[5] = data.data[4]
		self.data_raw.data[6] = data.data[5]
		self.data_raw.data[7] = data.data[6]
		self.data_raw.data[8] = data.data[7]

	def cbForceX(self,data):
		self.data_raw.data[9] = data.data
	
		
	
if __name__ == "__main__":
	try:
		run = emg_raw()
		rospy.spin()
	except rospy.ROSInterruptException:
		pass