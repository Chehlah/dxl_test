# step_run

# run connect port
sudo chmod a+rw /dev/ttyUSB0
sudo chmod a+rw /dev/input/js0 
>> set joy Logitech to "Xinput"

# launch
roslaunch dxl_test control_manager.launch
roslaunch dxl_test start_controller.launch
					
# topic
rostopic pub -1 /joint_2_controller/command std_msgs/Float64 ""data: 0.1""
>> Note: got a bug when run topic echo then service not active!!!

# rosservice
rosservice call /joint_1_controller/set_compliance_slope "slope: 10"
rosservice call /joint_2_controller/set_compliance_slope "slope: 10"

# Node
rosrun dxl_test jacobian_class.py
				
# บันทึกวันที่ 14/04/2019
1. part ข้อต่อ link 1 กับ joint 1 หัก/เสียหาย ระหว่างทดลอง velocity control with Jacobian 
>> ความเสียหายเกิดจาก inverted pendulum + overshoot velocity (มีกราฟ ขึ้นไป 5rad/s) 
>> ยังไม่แน่ใจว่า เป็นเพราะ 1. การคำนวนผิดพลาด/error หรือ 2. Dynamixel ยังไม่ tune PID
2. เปลี่ยน part ที่เสียหาย
3. ค่า PID ของ Dynamixel ไม่มีใน pkg ROS 
>> pkg Dynamixel motor : มี service slope เข้าใจว่าแทนตัว P controller
>> Dynamixel SDK : ยังไม่ลอง
4. ทำการปรับค่า service slope 
>> slope ที่ 10 ช่วยทำให้ joint คท smooth มากขึ้น
>> slope ที่ 1 motor ไม่มีแรงยกที่จะเข้า set point
5. เขียน code kinematic 
>> forward & inverse kinemtic function คำนวนได้ถูกต้อง
>> ต้องคำนึงถึง singularity เพราะแม้แต่สมการ inverse ที่ j1=j2=0 มี math error

# บันทึกวันที่ 21/04/2019
1. plot current pos ของ joint 2 มีปัญหา ค่า feedback มี noise 
>> ได้ข้อสรุปว่า ต้องเปลี่ยน motor เพราะส่งผลต่อการคำนวนสมการทั้งหมด 

# 23/04/2019
1. filter noise ได้แล้ว
2. plot XZ 2 graph 
>> position control with inverse kinematics
>> velocity control with inverse differential kinematics


