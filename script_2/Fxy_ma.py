#!/usr/bin/env python

import rospy
from geometry_msgs.msg import WrenchStamped
from std_msgs.msg import Float64
import numpy as np

class force_ma():
	stack_force = []
	limit = 100  # Edit window size
	npop  = 5    # pop size 
	n_pub = 1
	fx_ma = Float64()

	def __init__(self):
		rospy.init_node('force_ma', anonymous=True)
		rospy.Subscriber("/ft_sensor/netft_data", WrenchStamped , self.cbFTsensor)
		self.pubFx = rospy.Publisher("/Fx_ma", Float64 , queue_size=10)

		self.str_ = ("start : limit {}".format(self.limit))
		rospy.loginfo(self.str_)

	def cbFTsensor(self,data):
		self.stack_force.append(data.wrench.force.x)
		
		if len(self.stack_force) > self.limit:
			self.moving_average(self.stack_force[0:self.limit],self.limit)
			self.stack_force.pop(0) 
			# del self.stack_force[0:self.npop] 
		else:
			# self.fx_ma.data = data.wrench.force.x + 4      # upscale
			# self.pubFx.publish(self.fx_ma)
			pass

	def moving_average(self,dataSets,periods):
		weights = np.ones(periods) / periods
		ans = np.convolve(np.asarray(dataSets), weights, mode='valid')
		self.fx_ma.data = ans[0] + 4                        # upscale
		if self.n_pub == self.npop: 
			self.pubFx.publish(self.fx_ma)
			self.n_pub = 1
		else:
			self.n_pub = self.n_pub + 1
		# str_ = (">>>>> Limit {} ANS {}".format(self.limit,ans))
		# rospy.loginfo(str_)

if __name__ == "__main__":
	try:
		run = force_ma()
		rospy.spin()
	except rospy.ROSInterruptException:
		pass