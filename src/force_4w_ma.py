#!/usr/bin/env python

import rospy
from geometry_msgs.msg import WrenchStamped
from geometry_msgs.msg import Wrench
from std_msgs.msg import Float64
import numpy as np

class force_ma():
	stack_force = []
	stack_F_xyz = []
	limit = 5    # Edit window size
	npop  = 2    # pop size 
	n_pub = 1   
	fx_ma = Float64()
	fxyz_ma = []
	fxyz_ma_pub = Wrench()

	def __init__(self):
		rospy.init_node('force_4w_ma', anonymous=True)
		rospy.Subscriber("/ft_sensor/netft_data", WrenchStamped , self.cbFTsensor_XYZ)
		# self.pubFx   = rospy.Publisher("/fx_ma", Float64 , queue_size=10)
		self.pubFxyz = rospy.Publisher("/fxyz_ma", Wrench , queue_size=10)
		
		self.fxyz_ma = [0.0]*3
		self.str_ = ("start : limit {}".format(self.limit))
		rospy.loginfo(self.str_)

	def cbFTsensor_XYZ(self,data):
		self.stack_F_xyz.append([data.wrench.force.x,data.wrench.force.y,data.wrench.torque.z])

		if len(self.stack_F_xyz) > self.limit:
			self.moving_average_2(self.stack_F_xyz[0:self.limit],self.limit)
			self.stack_F_xyz.pop(0)  
		else:
			pass

	def moving_average_2(self,dataSets,periods):
		stack = []
		weights = np.ones(periods) / periods   # weight of MA
		for nn in range(0,3):  # loop of Multiple-signal
			stack.append([i[nn] for i in dataSets])
			# print(stack)
			self.fxyz_ma[nn] = np.convolve(np.asarray(stack[nn]), weights, mode='valid')
		if self.n_pub == self.npop: 
			self.fxyz_ma_pub.force.x  = self.fxyz_ma[0]
			self.fxyz_ma_pub.force.y  = self.fxyz_ma[1]
			self.fxyz_ma_pub.force.z  = 0.0
			self.fxyz_ma_pub.torque.x = 0.0
			self.fxyz_ma_pub.torque.y = 0.0
			self.fxyz_ma_pub.torque.z = self.fxyz_ma[2]
			self.pubFxyz.publish(self.fxyz_ma_pub)
			self.n_pub = 1
		else:
			self.n_pub = self.n_pub + 1


	# def cbFTsensor(self,data):
	# 	self.stack_force.append(data.wrench.force.x)
		
	# 	if len(self.stack_force) > self.limit:
	# 		self.moving_average(self.stack_force[0:self.limit],self.limit)
	# 		self.stack_force.pop(0) 
	# 		# del self.stack_force[0:self.npop] 
	# 	else:
	# 		# self.fx_ma.data = data.wrench.force.x + 4      # upscale
	# 		# self.pubFx.publish(self.fx_ma)
	# 		pass

	# def moving_average(self,dataSets,periods):
	# 	weights = np.ones(periods) / periods
	# 	ans = np.convolve(np.asarray(dataSets), weights, mode='valid')
	# 	self.fx_ma.data = ans[0]                        # upscale
	# 	if self.n_pub == self.npop: 
	# 		self.pubFx.publish(self.fx_ma)
	# 		self.n_pub = 1
	# 	else:
	# 		self.n_pub = self.n_pub + 1
	# 	# str_ = (">>>>> Limit {} ANS {}".format(self.limit,ans))
	# 	# rospy.loginfo(str_)

if __name__ == "__main__":
	try:
		run = force_ma()
		rospy.spin()
	except rospy.ROSInterruptException:
		pass