#!/usr/bin/env python
''' 
	Test function 
	- in 1 out 5
	- in 5 out 1
	
	Resolution:
	1 bit = 0.09 deg and 0.002 rad

	For Dynamixel - Wheel mode
	DXL set: J1,J2 = 13,15

	# The motor position output in rawdata Encoder  
	rostopic echo /motor_states/pan_tilt_port/motor_states[0]/position
'''
import rospy
from dynamixel_msgs.msg import JointState
from dynamixel_msgs.msg import MotorStateList
from std_msgs.msg import Float64
from std_msgs.msg import Int8
from std_msgs.msg import Int32
from std_msgs.msg import Bool

import math as m

class feature_51:
	# Attributes
	data_speed = Float64()
	target = 2048  # bit
	dpith = 0.01  # radians
	dbit = 10
	SS = "start"

	# Motor state
	M1_bit = 0
	M2_bit = 0
	M1_bit_5 = 0
	M1_raw = 0
	M1_moving = False
	n_check = 0
	i_round = 2 # start near center 0 deg
	x_last = 0

	def __init__(self):
		rospy.init_node('feature_1to5', anonymous=True)
		self.subM1 = rospy.Subscriber("/wheel_1_controller/state", JointState , self.cbWheelStateM1)
		self.subM2 = rospy.Subscriber("/wheel_2_controller/state", JointState , self.cbWheelStateM2)
		self.subMotor = rospy.Subscriber("/motor_states/pan_tilt_port", MotorStateList , self.cbMotorstate)
		self.subTarget = rospy.Subscriber("/target", Float64, self.cbTarget)
		self.pubM1speed = rospy.Publisher("/wheel_1_controller/command", Float64 , queue_size=10)
		self.pubM2speed = rospy.Publisher("/wheel_2_controller/command", Float64 , queue_size=10)

	# callback 
	def cbTarget(self,data):
		self.target = self.deg_to_bit(float(data.data),1)

	def cbWheelStateM1(self,data):
		# pass
		if self.target-self.M1_bit > self.dbit:
			self.data_speed.data = 0.2
			self.pubM1speed.publish(self.data_speed)
		elif self.target-self.M1_bit < -self.dbit:
			self.data_speed.data = -0.2
			self.pubM1speed.publish(self.data_speed)
		else:
			self.data_speed.data = 0.0
			self.pubM1speed.publish(self.data_speed)

	def cbWheelStateM2(self,data):
		pass
		# if self.target-self.M2_bit > self.dbit:
		# 	self.data_speed.data = 0.2
		# 	self.pubM2speed.publish(self.data_speed)
		# elif self.target-self.M2_bit < -self.dbit:
		# 	self.data_speed.data = -0.2
		# 	self.pubM2speed.publish(self.data_speed)
		# else:
		# 	self.data_speed.data = 0.0
		# 	self.pubM2speed.publish(self.data_speed)
	
	def cbMotorstate(self,data):
		self.M1_raw = data.motor_states[0].position
		self.M2_bit = data.motor_states[1].position
		self.M1_moving = data.motor_states[0].moving
		self.M1_speed = data.motor_states[0].speed
		
		# main
		self.counter_i(int(self.M1_raw))
		self.function_1to5(int(self.M1_raw))
		self.function_5to1(self.M1_bit_5)

		# str_ = ("{} > M1 {} M2 {} I {}".format(self.target,self.M1_bit,self.M2_bit,self.i_round))
		# str_ = ("{} I {} M1 {} M1_5 {} Mlast {} dbit {}".format(self.SS,self.i_round,self.M1_bit,self.M1_bit_5,self.x_last,self.M1_bit-self.x_last))
		str_ = ("{} I {} MR {} M1 {} M1_5 {} T {}".format(self.SS,self.i_round,self.M1_raw,self.M1_bit,self.M1_bit_5,self.target))
		rospy.loginfo(str_)
	
	# Method
	def deg_to_bit(self,x,case):
		if case == 1:
			return int((4096/360.0)*(x + 180.0))
		elif case == 5:
			return int((4096/360.0)*((x*case) + 180.0)) 

	def function_1to5(self,x):
		self.M1_bit_5 = x + (self.i_round * 4096)

	def function_5to1(self,x):
		self.M1_bit = int(x/5.0)

	def counter_i(self,x):
		if self.M1_moving and self.n_check == 0:
			self.SS = "case 1"
			self.x_last = x
			self.n_check = 1
		elif not self.M1_moving and self.n_check == 1:
			self.SS = "case 2"
			self.n_check = 0
		
		if self.M1_speed > 0 and abs(x-self.x_last) > 4000: # CCW
			self.i_round = self.i_round + 1
			self.x_last = x
		elif self.M1_speed < 0 and abs(x-self.x_last) > 4000: # CW
			self.i_round = self.i_round - 1
			self.x_last = x
		elif self.M1_moving:
			self.SS = "case 3"
			self.x_last = x
	
	# Main
	

def main():
	run_51 = feature_51()
	rospy.spin()

if __name__ == '__main__':
	try:
		main()
	except rospy.ROSInterruptException:
		pass