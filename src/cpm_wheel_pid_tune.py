#!/usr/bin/env python
''' 
	Tune PI controller 
	Date: 26/12/2020
'''

import rospy
from dynamixel_msgs.msg import JointState
from std_msgs.msg import Float64
from sensor_msgs.msg import Joy
from geometry_msgs.msg import WrenchStamped
from dxl_test_msg.msg import rawForce
from std_msgs.msg import Bool

import math as m

class pid_tune:
	# Attributes
	data_ft = Float64()
	data_joy = Joy()
	data_F_raw = rawForce()
	
	target = 0.0    # radius
	current_pos = 0.0
	status_moving = False
	mass_handle = 0.46 # mass for gravity compensate
	round = 0

	mass_zero = 0.0
	dxl_speed = 0.4 # rad/s 
	data_speed = Float64()

	dfx = Float64() # feedback /joint1/state for PID controller  
	pid_en = Bool()
	setpoint = Float64()

	mode = 0.0
	mode_pub = Float64()

	def __init__(self):
		rospy.init_node('cpm_wheel_pid', anonymous=True)
		self.subFT  = rospy.Subscriber("/Fx_ma", Float64 , self.cbFTsensor)
		self.subJoy = rospy.Subscriber("/joy", Joy, self.cbGetJoy)
		self.subM1  = rospy.Subscriber("/joint_1_controller/state", JointState , self.cbJoinStateM1)

		self.pubForce    = rospy.Publisher("/force_raw_product", rawForce , queue_size=10)
		self.pubM1       = rospy.Publisher("/joint_1_controller/command", Float64 , queue_size=10)
		self.pubM1_const = rospy.Publisher("/pid_wheel_1/Omega_const/", Float64 , queue_size=10)

		self.pubPID_eneble = rospy.Publisher("/pid_wheel_1/pid_enable", Bool , queue_size=10)
		self.pubJ1_ft_State = rospy.Publisher("/pid_wheel_1/state", Float64 , queue_size=10)
		self.pubJ1_ft_setpoint = rospy.Publisher("/pid_wheel_1/setpoint", Float64 , queue_size=10)
		self.pubMode = rospy.Publisher("/pid_wheel_1/mode", Float64 , queue_size=10)				
		
		self.pid_en.data = False
		self.setpoint.data = 0.0
		self.data_F_raw.setpoint = self.setpoint.data
	
	# Method
	# subscribe function
	
	def cbGetJoy(self,data):
		self.data_joy = data
		if self.data_joy.buttons[0] == 1:
			self.target = m.radians(80) #1.5708 # 93 degree
			self.publish_M1()
		elif self.data_joy.buttons[1] == 1:
			self.target = m.radians(-10) # -5 degree
			self.publish_M1()
		elif self.data_joy.buttons[3] == 1:
			self.target = m.radians(0)
			self.publish_M1()
		elif self.data_joy.buttons[2] == 1:
			self.target = m.radians(90)
			self.publish_M1()
		elif self.data_joy.buttons[9] == 1:
			if self.mass_zero == 1.0:
				self.mass_zero = 0.0
			else:
				self.mass_zero = 1.0
		elif self.data_joy.buttons[5] == 1:
			if self.pid_en.data == True:
				self.pid_en.data = False
			else:
				self.pid_en.data = True
			self.pubPID_eneble.publish(self.pid_en)
			self.pubPID_eneble.publish(self.pid_en)
			self.pubJ1_ft_setpoint.publish(self.setpoint)
			self.pubJ1_ft_setpoint.publish(self.setpoint)
		elif self.data_joy.buttons[4] == 1:
			self.setpoint.data = 0.0
			self.pubJ1_ft_setpoint.publish(self.setpoint)
			self.pubJ1_ft_setpoint.publish(self.setpoint)
			self.data_F_raw.setpoint = self.setpoint.data
		elif self.data_joy.buttons[10] == 1:
			if self.mode == 0.0:
				self.mode = 1.0
				self.mode_pub.data = self.mode
			else:
				self.mode = 0.0
				self.mode_pub.data = self.mode
			# if self.mode < 4.0:
			# 	self.mode = self.mode + 1.0
			# 	self.mode_pub.data = self.mode
			# else:
			# 	self.mode = 0.0
			# 	self.mode_pub.data = self.mode
			self.pubMode.publish(self.mode_pub)
			self.pubMode.publish(self.mode_pub)
		elif self.data_joy.buttons[6] == 1:
			self.dxl_speed = self.dxl_speed - 0.1
		elif self.data_joy.buttons[7] == 1:
			self.dxl_speed = self.dxl_speed + 0.1
	
	def cbJoinStateM1(self,data):
		self.current_pos = data.current_pos
		self.pubM1_const.publish(self.data_speed)
		# stop at target 
		if abs(self.target-self.current_pos) <= 0.01: 
			self.status_moving = False
			self.pubOmega_M1(0.0)
			self.pubJ1_ft_setpoint.publish(self.setpoint)
		# if m.degrees(self.current_pos) < -45.0 or m.degrees(self.current_pos) > 91.0:
		# 	self.status_moving = False
		# 	self.pubOmega_M1(0.0)
		if self.mode == 2.0:
			self.pubOmega_M1(0.0)
		elif self.mode == 1.0: # not run force sensor
			self.dfx.data = data.velocity
			self.pubJ1_ft_State.publish(self.dfx)

	def publish_M1(self):
		# check CW by target - current position 
		if self.target-self.current_pos > 0:
			self.pubOmega_M1(self.dxl_speed)
			self.status_moving = False
		elif self.target-self.current_pos < 0:
			self.pubOmega_M1(-self.dxl_speed)
			self.status_moving = True
			self.round = self.round + 1 

	def pubOmega_M1(self,data):
		self.data_speed.data = data
		self.setpoint.data = data
		if self.pid_en.data == False:
			self.pubM1.publish(self.data_speed)
			self.pubJ1_ft_setpoint.publish(self.setpoint)
		elif self.pid_en.data == True and self.mode == 1.0:
			self.pubJ1_ft_setpoint.publish(self.setpoint)
		elif self.pid_en.data == True and self.mode != 1.0:
			self.pubM1_const.publish(self.data_speed)


	def cbFTsensor(self,data):
		self.data_ft = data
		self.publish_rawForce()

	def publish_rawForce(self):
		m_x =  (self.mass_handle * (m.cos((m.pi/2)-self.current_pos)-1)) * self.mass_zero

		self.data_F_raw.status_dxl = 1
		self.data_F_raw.degree_dxl = 90-m.degrees(self.current_pos)
		self.data_F_raw.fx_mav = self.data_ft.data -4 -m_x 

		self.dfx.data = -self.data_F_raw.fx_mav # negative means resis the robot movement

		self.pubForce.publish(self.data_F_raw)
		if self.mode != 1:
			self.pubJ1_ft_State.publish(self.dfx)

		str_ = ("Mode {:03.1f} Target {:2.4f} Deg {:07.4f} Fx {:09.4f} Mass0 {:03.1f} EN-PID {} Speed {:03.1f}"
			.format(self.mode,m.degrees(self.target),self.data_F_raw.degree_dxl,self.data_F_raw.fx_mav,self.mass_zero,self.pid_en.data,self.dxl_speed))
		rospy.loginfo(str_)
	
	# logical function
	def force_velocity_control(self):
		dfx = self.data_F_raw.fx_mav
		dxl_speed = self.dxl_speed
		if dfx >= 1.0 and dfx <= 2.0:
			dxl_speed = -(dxl_speed - (0.4 * (dfx-1.0)))
		elif dfx > 2.0 and dfx <= 3.0:
			dxl_speed = 0.0
		elif dfx > 3.0 and dfx <= 5.0:
			dxl_speed = 0.25 * (dfx-3.0)  
		elif dfx > 5.0:
			self.target = 1.5708
			self.publish_M1()
		else:
			dxl_speed = -dxl_speed
		self.pubOmega_M1(dxl_speed)

if __name__ == '__main__':
	try:
		run_ = pid_tune()
		rospy.spin()
	except rospy.ROSInterruptException:
		pass