#!/usr/bin/env python
# -*- coding: utf-8 -*-
# import library
import os 
import rospy
from std_msgs.msg import Float64
# Port USB setup name something
if os.name == 'nt':
	import msvcrt
	def getch():
		return msvcrt.getch().decode()
else:
	import sys, tty, termios
	fd = sys.stdin.fileno()
	old_settings = termios.tcgetattr(fd)
	def getch():
		try:
			tty.setraw(sys.stdin.fileno())
			ch = sys.stdin.read(1)
		finally:
			termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
		return ch
# Uses Dynamixel SDK library
from dynamixel_sdk import *                    

class dxl_controller():
	# Parameter  
	current_pos     = Float64()
	feedback_speed  = Float64()
	effort_speed    = Float64()
	control_effort  = 0.0
	velo_const      = 0.0
	
	# Control table address
	ADDR_MX_TORQUE_ENABLE      = 24               # Control table address is different in Dynamixel model
	ADDR_MX_MOVING_SPEED       = 32
	ADDR_MX_PRESENT_POSITION   = 36
	ADDR_MX_PRESENT_SPEED      = 38
	ADDR_MX_P_GAIN             = 28
	ADDR_MX_I_GAIN             = 27

	# Protocol version
	PROTOCOL_VERSION            = 1.0               # See which protocol version is used in the Dynamixel

	# Default setting
	DXL_ID                      = 1                 # Dynamixel ID : 1
	BAUDRATE                    = 1000000           # Dynamixel default baudrate : 57600
	DEVICENAME                  = '/dev/ttyUSB0'    # Check which port is being used on your controller   "/dev/ttyUSB0".encode('utf-8')
													# ex) Windows: "COM1"   Linux: "/dev/ttyUSB0" Mac: "/dev/tty.usbserial-*"
	TORQUE_ENABLE               = 1                 # Value for enabling the torque
	TORQUE_DISABLE              = 0                 # Value for disabling the torque
	P_GAIN                      = 28
	I_GAIN                      = 6

	# Position Control Parameters
	DXL_MINIMUM_POSITION_VALUE  = 10                # Dynamixel will rotate between this value
	DXL_MAXIMUM_POSITION_VALUE  = 4000              # and this value (note that the Dynamixel would not move when the position value is out of movable range. Check e-manual about the range of the Dynamixel you use.)
	DXL_MOVING_STATUS_THRESHOLD = 20                # Dynamixel moving status threshold
	index = 0
	dxl_goal_position = [DXL_MINIMUM_POSITION_VALUE, DXL_MAXIMUM_POSITION_VALUE]         # Goal position
	TARGET = 2048

	# Initialize PortHandler instance
	# Set the port path
	# Get methods and members of PortHandlerLinux or PortHandlerWindows
	portHandler = PortHandler(DEVICENAME)

	# Initialize PacketHandler instance
	# Set the protocol version
	# Get methods and members of Protocol1PacketHandler or Protocol2PacketHandler
	packetHandler = PacketHandler(PROTOCOL_VERSION)

	def __init__(self):
		
		rospy.init_node('dxl_motor', anonymous=True)
		rospy.Subscriber("/force_pi/control_effort", Float64 , self.cbControlEffort)
		rospy.Subscriber("/v_const", Float64 , self.cbVelocityConstant)
		self.pub_posi = rospy.Publisher("/current_pos" , Float64 , queue_size=10 )
		self.pub_velo = rospy.Publisher("/stateVelocity" , Float64 , queue_size=10 )
		self.pub_effort = rospy.Publisher("/effortVelocity" , Float64 , queue_size=10 )

		# Open port
		if self.portHandler.openPort():
			print("Succeeded to open the port")
		else:
			print("Failed to open the port")
			print("Press any key to terminate...")
			getch()
			quit()

		# Set port baudrate
		if self.portHandler.setBaudRate(self.BAUDRATE):
			print("Succeeded to change the baudrate")
		else:
			print("Failed to change the baudrate")
			print("Press any key to terminate...")
			getch()
			quit()

		# SET PI gain for Velocity Control
		self.set_P_GAIN()
		self.set_I_GAIN()
	
		# Enable Dynamixel Torque
		dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(self.portHandler, self.DXL_ID, self.ADDR_MX_TORQUE_ENABLE, self.TORQUE_ENABLE)
		if dxl_comm_result != COMM_SUCCESS:
			print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
		elif dxl_error != 0:
			print("%s" % self.packetHandler.getRxPacketError(dxl_error))
		else:
			print("Torque has been successfully enable")
	
	def set_P_GAIN(self):
		dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(self.portHandler, self.DXL_ID, self.ADDR_MX_P_GAIN, self.P_GAIN)
		# print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
		if dxl_comm_result != COMM_SUCCESS:
			print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
		elif dxl_error != 0:
			print("%s" % self.packetHandler.getRxPacketError(dxl_error))
		else:
			print("P GAIN has been successfully received")

	def set_I_GAIN(self):
		dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(self.portHandler, self.DXL_ID, self.ADDR_MX_I_GAIN, self.I_GAIN)
		# print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
		if dxl_comm_result != COMM_SUCCESS:
			print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
		elif dxl_error != 0:
			print("%s" % self.packetHandler.getRxPacketError(dxl_error))
		else:
			print("I GAIN has been successfully received")

	def cbControlEffort(self,data):
		self.control_effort = data.data
		
	def cbVelocityConstant(self,data):
		self.velo_const = data.data

	def get_position(self):
		# Read present position
		dxl_present_position, dxl_comm_result, dxl_error = self.packetHandler.read2ByteTxRx(self.portHandler, self.DXL_ID, self.ADDR_MX_PRESENT_POSITION)
		if dxl_comm_result != COMM_SUCCESS:
			print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
		elif dxl_error != 0:
			print("%s" % self.packetHandler.getRxPacketError(dxl_error))

		self.current_pos.data = (dxl_present_position * 0.088) -180
		self.pub_posi.publish(self.current_pos)
		return dxl_present_position
	
	def get_velocity(self):
		dxl_present_speed, dxl_comm_result, dxl_error = self.packetHandler.read2ByteTxRx(self.portHandler, self.DXL_ID, self.ADDR_MX_PRESENT_SPEED)
		if dxl_comm_result != COMM_SUCCESS:
			print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
		elif dxl_error != 0:
			print("%s" % self.packetHandler.getRxPacketError(dxl_error))

		# feedback velocity state
		offset = 5
		if dxl_present_speed == 0 or dxl_present_speed == 1024:
			self.feedback_speed.data =  dxl_present_speed * 0.114
		elif dxl_present_speed > 1023: # 5 is data offset
			self.feedback_speed.data = (1024 - dxl_present_speed -offset) * 0.114
		else:
			self.feedback_speed.data =  (dxl_present_speed +offset) * 0.114
		self.pub_velo.publish(self.feedback_speed)		 
		return dxl_present_speed
	
	def pub_speed(self, dxl_speed):
		# Write goal position
		dxl_comm_result, dxl_error = self.packetHandler.write2ByteTxRx(self.portHandler, self.DXL_ID, self.ADDR_MX_MOVING_SPEED, dxl_speed)
		if dxl_comm_result != COMM_SUCCESS:
			print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
		elif dxl_error != 0:
			print("%s" % self.packetHandler.getRxPacketError(dxl_error))
	
	def main_run(self):
		state_ = "_"
		dxl_speed = 0
		while 1:
			posi_ = self.get_position()
			velo_ = self.get_velocity()
			velo_effort = self.velo_const + self.control_effort
			self.effort_speed.data = velo_effort
			self.pub_effort.publish(self.effort_speed)
			velo_effort = int(round(velo_effort / 0.144))
			if velo_effort < 0:
				dxl_speed = 1024 + abs(velo_effort)
				state_ = " CCW"
			else:
				dxl_speed = velo_effort
				state_ = " CW "
			self.pub_speed(dxl_speed)
			str_ = ("State [{}] Target {:05.2f} Postion {:05.2f} Effort {:02d} Velo {}".format(state_,(self.TARGET*0.088)-180,(posi_*0.088)-180,velo_effort,velo_))
			rospy.loginfo(str_)
		 
if __name__ == '__main__':
	try:
		run_dxl = dxl_controller()	
		run_dxl.main_run()
		rospy.spin()
	except rospy.ROSInterruptException:
		pass