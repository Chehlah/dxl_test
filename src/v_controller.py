#!/usr/bin/env python
'''
	This code was created for summing the velocity before publish to Dynamixel motor

	Note: Code is working with cpm_wheel_pid.py lookup overall step run at "step_run_cpm_wheel_pid.md" 
'''

import rospy
from std_msgs.msg import Float64
from dynamixel_msgs.msg import JointState
from std_msgs.msg import Float32MultiArray
from dxl_test_msg.msg import rawForce

import math as m

class v_controller:
	# Attributes
	omega_const = 0.0
	omega_PID = 0.0
	omega_pub = Float64()
	current_pos = 0.0
	setpoint = 0.0
	mode = 0.0

	output_velocity = Float32MultiArray()

	def __init__(self):
		rospy.init_node('v_controller', anonymous=True)
		self.subM1 = rospy.Subscriber("/joint_1_controller/state", JointState , self.cbJoinStateM1)
		self.subOmega_PID = rospy.Subscriber("/pid_wheel_1/control_effort", Float64 , self.cbOmega_PID)
		self.subSetpoint = rospy.Subscriber("/pid_wheel_1/setpoint/", Float64 , self.cbSetpoint_PID)
		self.subOmega_const = rospy.Subscriber("/pid_wheel_1/Omega_const/", Float64 , self.cbOmega_const)
		self.subMode = rospy.Subscriber("/pid_wheel_1/mode", Float64 , self.cbMode)
		self.subStateForce = rospy.Subscriber("/force_raw_product", rawForce , self.cbStateForce)

		self.pubM1 = rospy.Publisher("/joint_1_controller/command", Float64 , queue_size=10)
		self.pub_rec = rospy.Publisher("/output/rec/velocity" , Float32MultiArray , queue_size=10 )

		self.output_velocity.data = [0.0]*5


	# Method
	# Subscibe function
	def cbJoinStateM1(self,data):
		self.current_pos = data.current_pos
		self.output_velocity.data[2] = m.fabs(data.velocity)
		self.output_velocity.data[3] = m.degrees(data.current_pos)

	def cbSetpoint_PID(self,data):
		self.setpoint = data.data
		self.output_velocity.data[0] = m.fabs(self.setpoint)

	def cbOmega_const(self,data):
		self.omega_const = data.data
		if self.mode == 0.0:
			self.output_velocity.data[1] = m.fabs(self.omega_const)
		self.pub_rec.publish(self.output_velocity)
		str_ = ("Posi {:2.3f} Mode {:2.2f} Omega_Const {:02.2f} Omega_PID {:02.3f} ".format(m.degrees(self.current_pos),self.mode,self.omega_const,self.omega_PID))
		rospy.loginfo(str_)

	def cbMode(self,data):
		self.mode = data.data
	
	def cbStateForce(self,data):
		self.output_velocity.data[4] = data.fx_mav

	def cbOmega_PID(self,data):
		self.omega_PID = data.data

		if m.degrees(self.current_pos) < -45.0 or m.degrees(self.current_pos) > 91.5:
			self.pubspeed_M1(0.0)
		elif self.mode == 1.0: # just velocity
			if self.setpoint == 0.0:
				self.pubspeed_M1(0.0)
			else:
				self.pubspeed_M1(self.omega_PID)
			# self.pubspeed_M1(self.omega_PID)
			self.output_velocity.data[1] = m.fabs(self.omega_PID)
		elif self.mode == 3.0: # CW with load
			self.pubspeed_M1(self.omega_PID)
			self.output_velocity.data[1] = self.omega_PID
		elif self.mode == 2.0: # CCW with or without load
			if self.omega_const == 0.0 or self.omega_const == 0.4:
				self.pubspeed_M1(self.omega_const)
				self.output_velocity.data[1] = self.omega_const
			else:
				self.pubspeed_M1(self.omega_PID+self.omega_const)
				self.output_velocity.data[1] = self.omega_PID+self.omega_const
		self.pub_rec.publish(self.output_velocity)

		# str_ = ("Pos {:2.3f} State {:2.2f} Omega_Const {:02.2f} Omega_PID {:02.3f} ".format(m.degrees(self.current_pos),self.mode,self.setpoint,self.omega_PID))
		# rospy.loginfo(str_)

	def pubspeed_M1(self,data):
		self.omega_pub.data = data
		self.pubM1.publish(self.omega_pub)
		

if __name__ == '__main__':
	try:
		run_v = v_controller()
		rospy.spin()
	except rospy.ROSInterruptException:
		pass