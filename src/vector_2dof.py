#!/usr/bin/env python
'''
	Date: 28-11-2019
	Force-Velocity control for 2DOF Robot Arm 
	
	Note: step_run_vecolity.md
	Mode:   1- Joint-Position control - position control of each robot joint in DXL wheel mode
			*- Direction control - control the velocity of end-effector
			2- Path control      - control the velocity and path planning of end-effector
			4- Force-Velocity control - control end-effector via F/T sensor
'''
import rospy
from dynamixel_msgs.msg import JointState
from dynamixel_msgs.msg import MotorStateList
from std_msgs.msg import Float64
from std_msgs.msg import Int8
from std_msgs.msg import Int32
from std_msgs.msg import Bool
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Point
from geometry_msgs.msg import WrenchStamped
import math as m

class vector_2dof:
	# Attributs
	# callback
	M1_raw = 0.0
	M1_moving = False
	M1_speed = 0
	M2_raw = 0.0
	M2_moving = False
	M2_speed = 0
	# publish
	omega_M1 = Float64()
	omega_M2 = Float64()
	posePoint = Point()
	# Inverse Jacobian
	jM1 = 0.002
	jM2 = 0.002
	omega1 = 0.0
	omega2 = 0.0
	Vx = 0.0
	Vz = 0.0
	# Forward kinematic
	point_z = 0.0
	point_x = 0.0
	L1 = 0.4
	L2 = 0.4
	# command 
	target_M1 = 2048
	target_M2 = 3072
	mode_n = 0
	# I round check
	n1_check = 0
	n2_check = 0
	i1_round = 2  # start at 0 deg
	i2_round = 3  # start at 90 deg
	x1_last = 0
	x2_last = 0
	SS = "---"
	# visual encoder
	M1_bit = 10240  # start at 0 deg
	M2_bit = 15360  # start at 90 deg
	# Position control
	dbit = 10
	data_speed = Float64()
	# Path & Direction control
	tpoint = {'x':0.4,'z':0.4}
	dpoint = 0.003 # 3 mm
	Vtheta = 0.0
	Vconst = 0.05 # m/s
	case = 0
	# Force velocity control
	fv_case = 0
	fx,fy,f = 0.0,0.0,0.0
	f_theta,f_Vtheta = 0.0,0.0
	dtheta = 5.0 # deg
	dforce = 2.0 # N

	def __init__(self):
		
		rospy.init_node('vector_2DOF', anonymous=True)
		
		self.subM1 = rospy.Subscriber("/wheel_1_controller/state", JointState , self.cbWheelStateM1)
		self.subM2 = rospy.Subscriber("/wheel_2_controller/state", JointState , self.cbWheelStateM2)
		self.subMotor = rospy.Subscriber("/motor_states/pan_tilt_port", MotorStateList , self.cbMotorstate)
		self.subFT = rospy.Subscriber("/ft_sensor/netft_data", WrenchStamped , self.cbFTsensor)

		self.subMode = rospy.Subscriber("/mode_select", Int8, self.cbModeSelect)
		self.subTargetJoint1 = rospy.Subscriber("/target_joint_1", Float64, self.cbTargetJoint1)
		self.subTargetJoint2 = rospy.Subscriber("/target_joint_2", Float64, self.cbTargetJoint2)
		self.subJoy = rospy.Subscriber("/joy", Joy, self.cbGetJoy)

		self.pubM1speed = rospy.Publisher("/wheel_1_controller/command", Float64 , queue_size=10)
		self.pubM2speed = rospy.Publisher("/wheel_2_controller/command", Float64 , queue_size=10)
		self.pubPoint = rospy.Publisher("/pose/point",Point,queue_size=10)
	
	# Method
	# callback
	def cbFTsensor(self,data):
		self.fx = data.wrench.force.x
		self.fy = data.wrench.force.y
		self.force_direction()
	
	def cbWheelStateM1(self,data):
		self.jM1 = m.radians(self.bit_to_deg(self.M1_bit)+2.0) # +2 offset
	def cbWheelStateM2(self,data):
		self.jM2 = m.radians(self.bit_to_deg(self.M2_bit)+2.0) # +2 offset

	def cbMotorstate(self,data):
		self.M1_raw = data.motor_states[0].position
		self.M1_moving = data.motor_states[0].moving
		self.M1_speed = data.motor_states[0].speed
		self.M2_raw = data.motor_states[1].position
		self.M2_moving = data.motor_states[1].moving
		self.M2_speed = data.motor_states[1].speed
		self.visual_encoder(int(self.M1_raw),1)
		self.visual_encoder(int(self.M2_raw),2)
		# main
		self.main_run()

	def cbTargetJoint1(self,data):
		self.target_M1 = self.deg_to_bit(float(data.data))
	def cbTargetJoint2(self,data):
		self.target_M2 = self.deg_to_bit(float(data.data))
	
	def cbModeSelect(self,data):
		self.mode_n = data.data
	def cbGetJoy(self,data):
		if data.buttons[4] == 1:   #LB
			self.mode_n = self.mode_n - 1
		elif data.buttons[5] == 1: #RB
			self.mode_n = self.mode_n + 1
		elif data.buttons[7] == 1: #start
			self.case = 1
		elif data.buttons[6] == 1: #back
			self.case = 0
			self.Vx = 0.0
			self.Vz = 0.0
			self.omega1 = 0.0
			self.omega2 = 0.0
			self.publishSpeed()
		elif data.buttons[1] == 1: #B
			self.case = 99
			self.tpoint['x'] = 0.4
			self.tpoint['z'] = 0.4
		elif data.buttons[0] == 1: #A
			self.case = 99
			self.tpoint['x'] = 0.2
			self.tpoint['z'] = 0.2
		elif data.buttons[2] == 1: #X
			self.case = 99
			self.tpoint['x'] = 0.5
			self.tpoint['z'] = 0.2
		elif data.buttons[3] == 1: #Y
			self.mode_n = 0
			self.case = 0
			self.Vx = 0.0
			self.Vz = 0.0
			self.omega1 = 0.0
			self.omega2 = 0.0
			self.publishSpeed()
	
	# Publisher
	def publishSpeed(self):
		self.omega_M1.data = self.omega1*5 # speed 1:5
		self.pubM1speed.publish(self.omega_M1)
		self.omega_M2.data = self.omega2*5
		self.pubM2speed.publish(self.omega_M2)	
	
	def publishPosition(self):
		self.posePoint.x = self.point_x
		self.posePoint.y = 0.0
		self.posePoint.z = self.point_z
		self.pubPoint.publish(self.posePoint)

	# Calculation
	# degree to bit
	def deg_to_bit(self,x):
		return int((4096/360.0)*(x + 180.0)) 
	def bit_to_deg(self,x):
		return ((360.0/4096)*x)-180.0

	# encoder feedback of robot joint
	def visual_encoder(self,x,case): 
		# I round check 
		if case == 1:
			if self.M1_moving and self.n1_check == 0:
				self.SS = "Move-1::N-0"
				self.x1_last = x
				self.n1_check = 1
			elif not self.M1_moving and self.n1_check == 1:
				self.SS = "Move-0::N-1"
				self.n1_check = 0
			if self.M1_speed > 0 and abs(x-self.x1_last) > 4000: # CCW
				self.i1_round = self.i1_round + 1
				self.x1_last = x
			elif self.M1_speed < 0 and abs(x-self.x1_last) > 4000: # CW
				self.i1_round = self.i1_round - 1
				self.x1_last = x
			elif self.M1_moving:
				self.SS = "Move-1::N-XX"
				self.x1_last = x
			# encoder up-scale 1 to 5 and 5 to 1
			self.M1_bit = int((x + (self.i1_round * 4096))/5.0)
		elif case == 2:
			if self.M2_moving and self.n2_check == 0:
				self.SS = "Move-1::N-0"
				self.x2_last = x
				self.n2_check = 1
			elif not self.M2_moving and self.n2_check == 1:
				self.SS = "Move-0::N-1"
				self.n2_check = 0
			if self.M2_speed > 0 and abs(x-self.x2_last) > 4000: # CCW
				self.i2_round = self.i2_round + 1
				self.x2_last = x
			elif self.M2_speed < 0 and abs(x-self.x2_last) > 4000: # CW
				self.i2_round = self.i2_round - 1
				self.x2_last = x
			elif self.M2_moving:
				self.SS = "Move-1::N-XX"
				self.x2_last = x
			# encoder up-scale 1 to 5 and 5 to 1
			self.M2_bit = int((x + (self.i2_round * 4096))/5.0)
	
	# Math Calculation
	def forwardKinematic(self):
		self.point_z = (self.L1*m.cos(self.jM1))+(self.L2*m.cos(self.jM1+self.jM2))
		self.point_x = (self.L1*m.sin(self.jM1))+(self.L2*m.sin(self.jM1+self.jM2))

	def inverseJacobian(self):
		self.omega1 = (((m.cos(self.jM1+self.jM2)*self.Vz)+(m.sin(self.jM1+self.jM2)*self.Vx))
			/(m.sin(self.jM2)*self.L1))
		self.omega2 = ((((self.L1*m.cos(self.jM1)+self.L2*m.cos(self.jM1+self.jM2))*self.Vz)
			+(self.Vx*(self.L1*m.sin(self.jM1)+self.L2*m.sin(self.jM1+self.jM2))))
			/(-(self.L1*self.L2*m.sin(self.jM2))))

	def force_direction(self): # transform sensor frame to velocity frame
		self.f = m.sqrt(m.pow(self.fx,2)+m.pow(self.fy,2))
		f_theta = m.atan2(self.fy,self.fx)
		# f_theta = m.atan2(self.fx,self.fx)
		y = m.degrees(f_theta) + 90 + m.degrees(self.jM1+self.jM2) 
		# real use: offset theta [0:360] > [+180:0:-180]
		if y > 180:
			self.f_Vtheta = y - 360
		else:
			self.f_Vtheta = y
		# self.f_theta = m.degrees(self.Vtheta) - 90 - m.degrees(self.jM1+self.jM2)

	# Sub function
	def update_vector(self):
		x = self.tpoint['x'] - self.point_x
		z = self.tpoint['z'] - self.point_z
		if abs(x) <= self.dpoint and abs(z) <= self.dpoint:
			self.Vx = 0.0
			self.Vz = 0.0
		else:
			self.Vtheta = m.atan2(x,z)					#(y,x)
			self.Vx = self.Vconst * m.sin(self.Vtheta)	#cos
			self.Vz = self.Vconst * m.cos(self.Vtheta)	#sin 
	
	def force_control(self):
		if self.mode_n == 3:
			x = self.tpoint['x'] - self.point_x
			z = self.tpoint['z'] - self.point_z
			self.Vtheta = m.atan2(x,z)					#(y,x)
			if abs(x) <= self.dpoint and abs(z) <= self.dpoint:
				self.Vx = 0.0
				self.Vz = 0.0
			elif self.f >= self.dforce and self.f_Vtheta >= (m.degrees(self.Vtheta)-self.dtheta) and self.f_Vtheta <= (m.degrees(self.Vtheta)+self.dtheta):
				self.Vx = self.Vconst * m.sin(self.Vtheta)	#cos
				self.Vz = self.Vconst * m.cos(self.Vtheta)	#sin
			else:
				self.Vx = 0.0
				self.Vz = 0.0
		elif self.mode_n == 4:
			if self.f >= self.dforce:
				self.Vtheta = self.f_Vtheta
				self.Vx = self.Vconst * m.sin(self.Vtheta)	#cos
				self.Vz = self.Vconst * m.cos(self.Vtheta)	#sin
			else:
				self.Vx = 0.0
				self.Vz = 0.0

	# Mode 2: Path control
	def path_control(self):
		str_ = "---"
		if self.case == 1: # linear point to point 
			self.update_vector()
			self.inverseJacobian()
			# str_ = ("T {} P {: 3.3f}{: 3.3f} Omg {: 3.3f}{: 3.3f}".format(
			# 	[self.tpoint['x'],self.tpoint['z']],self.point_x,self.point_z,self.omega1,self.omega2))
			str_ = ("T {} P {: 3.3f}{: 3.3f} tV/tF {: 3.3f}{: 3.3f}".format(
				[self.tpoint['x'],self.tpoint['z']],self.point_x,self.point_z,m.degrees(self.Vtheta),self.f_Vtheta))
		elif self.case == 99:
			str_ = ("T {} P {: 3.3f}{: 3.3f} tV/tF {: 3.3f}{: 3.3f}".format(
				[self.tpoint['x'],self.tpoint['z']],self.point_x,self.point_z,m.degrees(self.Vtheta),self.f_Vtheta))
			# str_ = ("T {} P {: 3.3f}{: 3.3f} Omg {: 3.3f}{: 3.3f}".format(
			# 	[self.tpoint['x'],self.tpoint['z']],self.point_x,self.point_z,self.omega1,self.omega2))
		else:
			str_ = ("Mode 2 - Waiting for start case M_raw {} tV/tF {: 3.3f}{: 3.3f} F {: 3.3f}".format(
				[self.M1_raw,self.M2_raw],m.degrees(self.Vtheta),self.f_Vtheta,self.f))			
		self.publishSpeed()
		return str_

	# Mode 3-4: Force-Velocity control
	def force_velocity_control(self):
		str_ = "---"
		if self.case == 1:
			self.force_control()
			self.inverseJacobian()
			str_ = ("c1 T {} P {: 3.3f}{: 3.3f} tV {: 3.3f} tF {: 3.3f} F {: 3.3f}".format(
				[self.tpoint['x'],self.tpoint['z']],self.point_x,self.point_z,m.degrees(self.Vtheta),self.f_Vtheta,self.f))
		elif self.case == 99:
			str_ = ("c99 T {} P {: 3.3f}{: 3.3f} tV {: 3.3f} tF {: 3.3f} F {: 3.3f}".format(
				[self.tpoint['x'],self.tpoint['z']],self.point_x,self.point_z,m.degrees(self.Vtheta),self.f_Vtheta,self.f))
		else:
			if self.mode_n == 3:
				str_ = ("Mode 3 - Waiting for start case")
			elif self.mode_n == 4:
				str_ = ("Mode 4 - Waiting for start case")
		self.publishSpeed()
		return str_

	# Mode 1: Position control
	def joint_position_control(self):
		if self.case == 1:
			if self.target_M1-self.M1_bit > self.dbit:
				self.data_speed.data = 0.2
				self.pubM1speed.publish(self.data_speed)
			elif self.target_M1-self.M1_bit < -self.dbit:
				self.data_speed.data = -0.2
				self.pubM1speed.publish(self.data_speed)
			else:
				self.data_speed.data = 0.0
				self.pubM1speed.publish(self.data_speed)

			if self.target_M2-self.M2_bit > self.dbit:
				self.data_speed.data = 0.2
				self.pubM2speed.publish(self.data_speed)
			elif self.target_M2-self.M2_bit < -self.dbit:
				self.data_speed.data = -0.2
				self.pubM2speed.publish(self.data_speed)
			else:
				self.data_speed.data = 0.0
				self.pubM2speed.publish(self.data_speed)

			return ("SS {} T1 {} M1 {} T2 {} M2 {}".format(self.SS,self.target_M1,self.M1_bit,self.target_M2,self.M2_bit))
		else:
			return ("Mode 1 - Waiting for start case : M_raw {}".format([self.M1_raw,self.M2_raw]))

	# Main
	def main_run(self):
		str_ = "---"
		self.forwardKinematic()
		self.publishPosition()
		# self.force_direction()
		if self.mode_n == 1: # position joint control
			str_ = self.joint_position_control()
		elif self.mode_n == 2:
			str_ = self.path_control()
		elif self.mode_n == 3 or self.mode_n == 4: 
			str_ = self.force_velocity_control()
		else:
			# str_ = ("Please select mode to control a robot! M_raw {} tV/tF {: 3.3f}{: 3.3f}".format(
			# 	[self.M1_raw,self.M2_raw],m.degrees(self.Vtheta),self.f_Vtheta))
			str_ = ("Please select mode to control a robot! M_raw {} J1/J2 {: 3.3f}{: 3.3f}".format(
				[self.M1_raw,self.M2_raw],m.degrees(self.jM1),m.degrees(self.jM2)))
		rospy.loginfo(str_)


def main():
	run_vector = vector_2dof()
	rospy.spin()

if __name__ == '__main__':
	try:
		main()
	except rospy.ROSInterruptException:
		pass