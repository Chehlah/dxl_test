#!/usr/bin/env python
'''
	Velocity control at end-effector

	set dynamixel in wheel mode
	!!! be carefull !!!

	Setting Day 04/08/2019
	ID 15,12 J1,J2

	Project: EP-0408-01

'''

import rospy
from dynamixel_msgs.msg import JointState
from std_msgs.msg import Float64
from std_msgs.msg import Int8
from sensor_msgs.msg import Joy
from geometry_msgs.msg import PointStamped

import math as m

class jacobian2dof:
	# Attributes
	# param for jacobian
	jM1 = 0.0015
	jM2 = 0.0015
	omega1 = 0.0
	omega2 = 0.0

	directionX = 0
	L1 = 1.0 		# mm
	L2 = 1.0
	Vconst = 0.05	# 0.05
	Vx = 0.0 		# m/s
	Vz = 0.0

	getJoy = Joy()
	data1 = Float64()
	data2 = Float64()
	
	pointS = PointStamped()

	# command position 
	start_case = 0
	i_case = 0
	p_x = L2 # 0.345   # m  #start point
	p_z = L1 # 0.41
	p_x_a = 10.0/100       # cm
	p_z_a = 50.0/100        
	p_x_b = p_z_b = p_x_a
	p_x_c,p_z_c = p_z_a, p_x_a
	p_x_d = p_z_d = p_z_a

	# plot graph
	plot_x = 0.0
	plot_z = 0.0

	# low pass filter param
	Xold_joint1 = 0.0
	Xold_joint2 = 0.0
	lpf_case1 = 1
	lpf_case2 = 1
	data_out_M1 = 0.0
	data_out_M2 = 0.0
	
	def __init__(self):
		rospy.init_node('jacobian_2dof', anonymous=True)
		
		self.subM1 = rospy.Subscriber("/wheel_1_controller/state", JointState , self.cbWheelStateM1)
		self.subM2 = rospy.Subscriber("/wheel_2_controller/state", JointState , self.cbWheelStateM2)
		self.subDirec = rospy.Subscriber("/directionX", Int8, self.cbDirectionX)
		self.subJoy = rospy.Subscriber("/joy", Joy, self.cbGetJoy)

		self.pubM1speed = rospy.Publisher("/wheel_1_controller/command", Float64 , queue_size=10)
		self.pubM2speed = rospy.Publisher("/wheel_2_controller/command", Float64 , queue_size=10)
		self.pubPx = rospy.Publisher("/fwd/Px", Float64 , queue_size=10)
		self.pubPz = rospy.Publisher("/fwd/Pz", Float64 , queue_size=10)
		self.pubCurM1 = rospy.Publisher("/filter/current_pos/M1", Float64 , queue_size=10)
		self.pubCurM2 = rospy.Publisher("/filter/current_pos/M2", Float64 , queue_size=10)
		self.pubPointStamped = rospy.Publisher("/pose/point",PointStamped,queue_size=10 )

		# pointStamped frame ID
		self.pointS.header.frame_id = 'eff'

	# Method
	def cbGetJoy(self,data):
		self.getJoy = data
		# str_ = ("get joy {} and {}".format(self.getJoy.axes[6],self.getJoy.axes[7]))
		# rospy.loginfo(str_)
		if self.getJoy.axes[0] == 1.0 and self.getJoy.axes[1] == 0.0:
			self.Vx = self.Vconst
		elif self.getJoy.axes[0] == -1.0 and self.getJoy.axes[1] == 0.0:
			self.Vx = -self.Vconst
		elif self.getJoy.axes[0] == 0.0 and self.getJoy.axes[1] == 1.0:
			self.Vz = self.Vconst
		elif self.getJoy.axes[0] == 0.0 and self.getJoy.axes[1] == -1.0:
			self.Vz = -self.Vconst
		
		# Start run auto regtangular movement 
		elif self.getJoy.buttons[1] == 1:
			self.i_case = 1	
		elif self.getJoy.buttons[2] == 1:
			self.i_case = 0
			self.Vx = 0.0
			self.Vz = 0.0
		else:
			self.Vx = 0.0
			self.Vz = 0.0

	def cbWheelStateM1(self,data):
		self.jM1 = data.current_pos
		self.publish_CurM1()
	
	def cbWheelStateM2(self,data):
		self.jM2 = data.current_pos
		self.publish_CurM2()
		self.main_run_invJ() # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

	def cbDirectionX(self,data):
		self.directionX = data.data
		if self.directionX == 1:
			self.Vx = self.Vconst
		elif self.directionX == -1:
			self.Vx = -self.Vconst
		else:
			self.directionX = 0
			self.Vx = 0.0
	
	def publishSpeed(self):
		self.data1.data = self.omega1
		self.pubM1speed.publish(self.data1)
		self.data2.data = self.omega2
		self.pubM2speed.publish(self.data2)

	def publish_Px(self):
		data = Float64()
		data.data = self.plot_x
		self.pubPx.publish(data)

	def publish_Pz(self):
		data = Float64()
		data.data = self.plot_z
		self.pubPz.publish(data)

	def publish_CurM1(self):
		data = Float64()
		if self.lpf_case1 == 1:
			self.Xold_joint1 = self.jM1
			self.lpf_case1 = 0
		# the value 0.02 is max range the data change in normal
		if abs(self.Xold_joint1 - self.jM1) < 0.1: 
			self.data_out_M1 = self.jM1
		else:
			self.data_out_M1 = self.Xold_joint1
		data.data = self.data_out_M1
		self.Xold_joint1 = self.data_out_M1
		self.jM1 = self.data_out_M1
		self.pubCurM1.publish(data)

	def publish_CurM2(self):
		data = Float64()
		if self.lpf_case2 == 1:
			self.Xold_joint2 = self.jM2
			self.lpf_case2 = 0
		if abs(self.Xold_joint2 - self.jM2) < 0.1: 
			self.data_out_M2 = self.jM2
		else:
			self.data_out_M2 = self.Xold_joint2
		data.data = self.data_out_M2
		self.Xold_joint2 = self.data_out_M2
		self.jM2 = self.data_out_M2
		self.pubCurM2.publish(data)

	def pointStamped(self):
		self.pointS.point.x = self.plot_x
		self.pointS.point.y = 0.0
		self.pointS.point.z = self.plot_z
		self.pubPointStamped.publish(self.pointS)

	def forwardKinematic(self):
		self.plot_z = (self.L1*m.cos(self.jM1))+(self.L2*m.cos(self.jM1+self.jM2))
		self.plot_x = (self.L1*m.sin(self.jM1))+(self.L2*m.sin(self.jM1+self.jM2))

	def inverseJacobian(self):
		# if self.jM2 >= 0.0015:
		self.omega1 = (((m.cos(self.jM1+self.jM2)*self.Vz)+(m.sin(self.jM1+self.jM2)*self.Vx))
			/(m.sin(self.jM2)*self.L1))
		self.omega2 = ((((self.L1*m.cos(self.jM1)+self.L2*m.cos(self.jM1+self.jM2))*self.Vz)
			+(self.Vx*(self.L1*m.sin(self.jM1)+self.L2*m.sin(self.jM1+self.jM2))))
			/(-(self.L1*self.L2*m.sin(self.jM2))))	
		# else:
		# 	self.omega1 = 0.0
		# 	self.omega2 = 0.0

		# for safety overshoot
		# if self.omega1 >= 1.0:
		# 	self.omega1 = 1.0
		# if self.omega2 >= 1.0:
		# 	self.omega2 = 1.0

	def controlPart_rectangular(self):
		if self.i_case == 1:
			self.Vx = 0.0
			self.Vz = -self.Vconst
			if abs(self.plot_z-self.p_z_b) <= 0.01:
				self.i_case = 2
		elif self.i_case == 2 :
			self.Vx = self.Vconst
			self.Vz = 0.0
			if abs(self.plot_x-self.p_x_c) <= 0.01:
				self.i_case = 3
		elif self.i_case == 3 :
			self.Vx = 0.0
			self.Vz = self.Vconst
			if abs(self.plot_z-self.p_z_d) <= 0.01:
				self.i_case = 4
		elif self.i_case == 4 :
			self.Vx = -self.Vconst
			self.Vz = 0.0
			if abs(self.plot_x-self.p_x_a) <= 0.01:
				self.i_case = 5
				self.Vz = 0.0
				self.Vx = 0.0
	
	def main_run_invJ(self):
		self.inverseJacobian()
		self.forwardKinematic()
		self.controlPart_rectangular() 
		# str_ = ("J1:{: 3.3f} J2:{: 3.3f} Omega1:{: 3.3f} Omega2:{: 3.3f} fM1{:.3f} fM2{:.3f}"
		# 	.format(self.jM1,self.jM2,self.omega1,self.omega2,self.data_out_M1,self.data_out_M2))		
		str_ = ("Case {} Px {:2.3f} Pz {:2.3f} Vx {:2.3f} Vz {:2.3f} O1 {:2.3f} O2 {:2.3f}"
			.format(self.i_case,self.plot_x,self.plot_z,self.Vx,self.Vz,self.omega1,self.omega2))
		rospy.loginfo(str_)
		self.publishSpeed()
		self.publish_Px()
		self.publish_Pz()
		self.pointStamped()

def main():
	run_jacobian = jacobian2dof()
	rospy.spin()

if __name__ == '__main__':
	try:
		main()
	except rospy.ROSInterruptException:
		pass