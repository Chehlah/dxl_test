#!/usr/bin/env python

import rospy
from geometry_msgs.msg import WrenchStamped
from std_msgs.msg import Float64
from std_msgs.msg import Bool
import numpy as np
import math as m
from sensor_msgs.msg import Joy
from std_msgs.msg import Float32MultiArray

class force_pi():
	stack_force = []
	limit = 200	  # Edit window size
	npop  = 10    # pop size 
	n_pub = 1
	setpoint = Float64()
	fx_ma = Float64()
	mass_handle = 0.46
	current_pos = 0.0
	data_joy = Joy()
	TARGET = (2048 * 0.088) - 180
	enable = Bool()
	output_velocity = Float32MultiArray()

	def __init__(self):
		rospy.init_node('force_pi', anonymous=True)
		rospy.Subscriber("/joy", Joy, self.cbGetJoy)
		rospy.Subscriber("/ft_sensor/netft_data", WrenchStamped , self.cbFTsensor)
		rospy.Subscriber("/current_pos", Float64 , self.cbCurrentPos)
		rospy.Subscriber("/force_pi/state", Float64 , self.cbForRec)
		self.pubSet = rospy.Publisher("/force_pi/setpoint", Float64 , queue_size=10)
		self.pubFx  = rospy.Publisher("/force_pi/Fx", Float64 , queue_size=10)
		self.pubEnable  = rospy.Publisher("/force_pi/pid_enable", Bool , queue_size=1)
		self.pub_rec  = rospy.Publisher("/output/rec/force_velocity" , Float32MultiArray , queue_size=10 )

		self.output_velocity.data = [0.0]*2
		self.str_ = ("start : limit {}".format(self.limit))
		rospy.loginfo(self.str_)

	def cbFTsensor(self,data):
		self.stack_force.append(data.wrench.force.x)
		
		if len(self.stack_force) > self.limit:
			self.moving_average(self.stack_force[0:self.limit],self.limit)
			self.stack_force.pop(0) 
		else:
			pass

	def moving_average(self,dataSets,periods):
		weights = np.ones(periods) / periods
		ans = np.convolve(np.asarray(dataSets), weights, mode='valid')
		# Mass handle compensate
		data_fx = ans[0] - (self.mass_handle * (m.cos((m.pi/2)-m.radians(self.current_pos))-1))                      
		if self.n_pub == self.npop:
			self.fx_ma.data = data_fx 
			self.pubFx.publish(self.fx_ma)
			# Real Setpoint of force to velocity
			self.setpoint.data = self.setpoint_scale(self.fx_ma.data)  
			self.pubSet.publish(self.setpoint)
			self.n_pub = 1
		else:
			self.n_pub = self.n_pub + 1

	def setpoint_scale(self,fx):
		if abs(self.TARGET - self.current_pos) < (100*0.088): # 1bit = 0.088deg
			return 0.0
		else:
			return 3.0 # 2.0 3.0 4.0 5.0 <return a setpoint>
 		# elif fx < 2: # rpm
		# 	return 2.0
		# elif fx >= 2 and fx <= 10:
		# 	return (0.5*fx)+1.0 
		# else:
		# 	return 6.0

	def cbCurrentPos(self,data):
		self.current_pos = data.data #
		self.str_ = ("Current Position = {}".format(self.current_pos))
		# rospy.loginfo(self.str_)
	
	def cbGetJoy(self,data):
		self.data_joy = data
		if   self.data_joy.buttons[0] == 1:
			self.TARGET = (2048 * 0.088) - 180 #  0
		elif self.data_joy.buttons[3] == 1:
			self.TARGET = (2958 * 0.088) - 180# +80
		elif self.data_joy.buttons[1] == 1:
			self.TARGET = (1900* 0.088) - 180 # -12
		elif self.data_joy.buttons[2] == 1:
			self.TARGET = (3070* 0.088) - 180 # +90
		elif self.data_joy.buttons[6] == 1:
			self.enable.data = False
			self.pubEnable.publish(self.enable)
		elif self.data_joy.buttons[7] == 1:
			self.enable.data = True
			self.pubEnable.publish(self.enable)
	
	def cbForRec(self,data):
		self.output_velocity.data[0] = self.setpoint.data 
		self.output_velocity.data[1] = data.data
		self.pub_rec.publish(self.output_velocity)

		
if __name__ == "__main__":
	try:
		run = force_pi()
		rospy.spin()
	except rospy.ROSInterruptException:
		pass