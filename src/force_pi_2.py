#!/usr/bin/env python

import rospy
from geometry_msgs.msg import WrenchStamped
from std_msgs.msg import Float64
from std_msgs.msg import Bool
import numpy as np
import math as m
from sensor_msgs.msg import Joy
from std_msgs.msg import Float32MultiArray

class force_pi():
	active_mode = True # False
	stack_force = []
	limit = 200	  # Edit window size
	npop  = 10    # pop size 
	n_pub = 1
	setpoint = Float64()
	v_const = Float64()
 	fx_ma = Float64()
	fx_com = Float64()
	fx_dfx = Float64()
	mass_handle = 0.84 # passive 0.84 active 0.XX
	current_pos = 0.0
	effortVelocity = 0.0
	data_joy = Joy()
	TARGET = (2048 * 0.088) - 180
	enable = Bool()
	output_velocity = Float32MultiArray()

	th_k = 2.0 # Force N
	mass_forearm  = 6.5 
	forcePredict  = 0.0
	forceMgCos    = 0.0
	forceFitCurve = 0.0
	fx_com_mgcos  = 0.0
	fx_com_fit    = 0.0
	fx_predict    = 0.0

	def __init__(self):
		rospy.init_node('force_pi', anonymous=True)
		rospy.Subscriber("/joy", Joy, self.cbGetJoy)
		rospy.Subscriber("/ft_sensor/netft_data", WrenchStamped , self.cbFTsensor)
		rospy.Subscriber("/current_pos", Float64 , self.cbCurrentPos)
		rospy.Subscriber("/stateVelocity", Float64 , self.cbForRec)
		rospy.Subscriber("/effortVelocity", Float64 , self.cbEffortVelocity) 
		rospy.Subscriber("/compensation/effort", Float64, self.cbMechineLearning) 
		self.pubSet    = rospy.Publisher("/force_pi/setpoint", Float64 , queue_size=10)
		self.pubVconst = rospy.Publisher("/v_const", Float64 , queue_size=10)
		self.pubFx     = rospy.Publisher("/Fx", Float64 , queue_size=10)
		self.pubFx_com = rospy.Publisher("/Fx/compensation", Float64 , queue_size=10)
		self.pubEnable = rospy.Publisher("/force_pi/pid_enable", Bool , queue_size=1)
		self.pub_rec   = rospy.Publisher("/output/rec/force_velocity" , Float32MultiArray , queue_size=10 )
		self.pubState  = rospy.Publisher("/force_pi/state", Float64 , queue_size=10)
		self.output_velocity.data = [0.0]*9
		self.str_ = ("start : limit {}".format(self.limit))
		rospy.loginfo(self.str_)
		### change mass handle
		if self.active_mode:
			self.str_ = ("Active Mode")
			self.th_k = 1.0
		else:
			self.str_ = ("Passive Mode")
			self.th_k = 2.0
		rospy.loginfo(self.str_)

	def cbFTsensor(self,data): # 200Hz
		self.stack_force.append(data.wrench.force.x)
		if len(self.stack_force) > self.limit:
			self.moving_average(self.stack_force[0:self.limit],self.limit)
			self.stack_force.pop(0) 
		else:
			pass

	def moving_average(self,dataSets,periods):
		weights = np.ones(periods) / periods
		ans = np.convolve(np.asarray(dataSets), weights, mode='valid')
		data_fx = ans[0]                    
		# Add compensation model + # Mass handle compensate
		F_predict = self.forcePredict       # ML
		F_fit     = self.forceFitCurve      # Fit curve
		F_mgcos   = self.forceMgCos         # MgCOS
		####
		mass_com = self.mass_handle * (m.cos((m.pi/2)-m.radians(self.current_pos))-1)
		data_fx_com_predict  = data_fx - F_predict - mass_com
		data_fx_com_fit	     = data_fx - F_fit     - mass_com
		data_fx_com_mgcos    = data_fx - F_mgcos   - mass_com
		####
		if self.active_mode:
			data_fx_compensation = data_fx - mass_com
		else:
			data_fx_compensation = data_fx_com_predict
		####
		if self.n_pub == self.npop: # 20Hz
			
			self.fx_com_mgcos = data_fx_com_mgcos
			self.fx_com_fit   = data_fx_com_fit
			self.fx_predict   = F_predict	

			self.fx_ma.data = data_fx - mass_com
			self.pubFx.publish(self.fx_ma) 

			self.fx_com.data = data_fx_compensation 
			self.pubFx_com.publish(self.fx_com) 
			
			self.fx_dfx.data = self.feetback_scale(data_fx_compensation)  
			self.pubState.publish(self.fx_dfx) 
			
			self.setpoint.data = 0.0 
			self.pubSet.publish(self.setpoint) 
			self.n_pub = 1 
		else:
			self.n_pub = self.n_pub + 1

	def feetback_scale(self,fx):		
		if self.active_mode:
			# Isokinetic active mode #
			
			if fx >= self.th_k: # transform to velocity offset 2 N 
				return -fx + self.th_k # V_effort (0.6*fx)
			elif fx <= -self.th_k:
				return -fx - self.th_k
			else:
				return 0.0
		else:
			if fx >= self.th_k: # transform to velocity offset 2 N 
				return -fx + self.th_k # V_effort (0.6*fx)
			else:
				return 0.0
		
	def cbCurrentPos(self,data):
		self.current_pos = data.data # deg
		x = 90-self.current_pos
		self.forceFitCurve   = self.mass_forearm * (2e-06*m.pow(x,3) - 2e-04*m.pow(x,2) - 3.9e-03*x + 1)
		self.forceMgCos		 = self.mass_forearm * m.cos(m.radians(x))

		# self.str_ = ("Current Position = {}".format(self.current_pos))
		# rospy.loginfo(self.str_)

		if abs(self.TARGET - self.current_pos) < (30*0.088): # 1bit = 0.088deg
			self.v_const.data = 0.0
		elif self.TARGET - self.current_pos > 0: # CCW down
			self.v_const.data = 3.82
		elif self.TARGET - self.current_pos < 0: # CW up
			self.v_const.data = -3.82
		self.pubVconst.publish(self.v_const)
	
	def cbGetJoy(self,data):
		self.data_joy = data
		if   self.data_joy.buttons[0] == 1:
			self.TARGET = (2048 * 0.088) - 180 #  0
		elif self.data_joy.buttons[3] == 1:
			self.TARGET = (2958 * 0.088) - 180# +80
		elif self.data_joy.buttons[1] == 1:
			self.TARGET = (1900* 0.088) - 180 # -12
		elif self.data_joy.buttons[2] == 1:
			self.TARGET = (3070* 0.088) - 180 # +90
		elif self.data_joy.buttons[6] == 1:
			self.enable.data = False
			self.pubEnable.publish(self.enable)
		elif self.data_joy.buttons[7] == 1:
			self.enable.data = True
			self.pubEnable.publish(self.enable)
	
	def cbEffortVelocity(self,data):
		self.effortVelocity = data.data

	def cbForRec(self,data):
		self.output_velocity.data[0] = 90-self.current_pos
		self.output_velocity.data[1] = data.data  # stateVelocity
		self.output_velocity.data[2] = self.effortVelocity
		self.output_velocity.data[3] = self.fx_ma.data 
		self.output_velocity.data[4] = self.fx_dfx.data 
		self.output_velocity.data[5] = self.fx_com.data #ML
		self.output_velocity.data[6] = self.fx_com_fit
		self.output_velocity.data[7] = self.fx_com_mgcos
		self.output_velocity.data[8] = self.fx_predict

		self.pub_rec.publish(self.output_velocity)
	
	def cbMechineLearning(self,data):
		self.forcePredict = data.data

		
if __name__ == "__main__":
	try:
		run = force_pi()
		rospy.spin()
	except rospy.ROSInterruptException:
		pass