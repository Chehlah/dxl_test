#!/usr/bin/env python
'''
	Class in rospy code 
	0.1 sup pub string
	1.0 test main loop w/ rate 
'''

import rospy, os, sys
from std_msgs.msg import String

class speechInOut:
	# attributes
	strGet = ""
	data = String()

	def __init__(self):
		rospy.init_node('rospyClass', anonymous=True)
		namein = rospy.get_param('~setName')
		self.pubStr = rospy.Publisher("/talker", String, queue_size=10)
		self.subStr = rospy.Subscriber("/listener", String, self.cbString, namein)

	# method
	def cbString(self, data, namein):
		self.data.data = data.data
		self.strGet = "Got string {} {}".format(self.data.data, namein)
		rospy.loginfo(self.strGet)
		self.pubTalker()
	
	def pubTalker(self):
		self.data.data = self.strGet
		self.pubStr.publish(self.data)

def main(args):
	# init class here
	speechIO = speechInOut()
	rospy.spin()   

if __name__ == "__main__":
	try:
		main(sys.argv)
	except rospy.ROSInterruptException:
		pass