#!/usr/bin/env python
'''
	test topic flow 
	
	results at 20Hz missing 1-2 topic at begin
'''
import rospy, os, sys
from std_msgs.msg import String

class speechInOut:
	# attributes
	strGet = ""
	data = String()
	j = 0

	def __init__(self):
		rospy.init_node('inout_hzControl', anonymous=True)
		self.pubStr = rospy.Publisher("/talker", String, queue_size=10)
		self.subStr = rospy.Subscriber("/listener", String, self.cbString)

	# method
	def cbString(self, data):
		self.data.data = data.data
		self.j = self.j + 1
		# if self.j < 5:
		self.strGet = "Got string {}".format(self.data.data)
		# print(self.strGet)
		rospy.loginfo(self.strGet)
		self.pubTalker()
	
	def pubTalker(self):
		self.data.data = self.strGet
		self.pubStr.publish(self.data)

def main(args):
	# init class here
	speechIO = speechInOut()
	rate = rospy.Rate(20) # Hz
	while not rospy.is_shutdown():
		speechIO.pubTalker()
		rate.sleep()
	# rospy.spin()   

if __name__ == "__main__":
	try:
		main(sys.argv)
	except rospy.ROSInterruptException:
		pass