#!/usr/bin/env python
'''
	Position and maybe Force-Velocity control in CPM mode.
	Control a robot for recording raw data samples of EMG, Theta and Force.
	setup Dynamixel in wheel mode.

	readme file name "step emg cpm"

	start: 13/03/2020
'''

import rospy
from std_msgs.msg import Float64MultiArray
from dynamixel_msgs.msg import JointState
from std_msgs.msg import Float64
from sensor_msgs.msg import Joy
from dxl_test_msg.msg import dataTrain
from geometry_msgs.msg import WrenchStamped
import math as m 

class cpm_emg:
	data_joy = Joy()
	target = 0.0 #radius
	dxl_speed = 0.40 # rad/s
	current_pos = 0.0
	data_speed = Float64()

	status_moving = False
	round = 0
	round_check = 0
	
	mass_zero = 8 # edit N
	mass_handle = 0.8
	str_ = ""

	force_x = Float64()

	data_train_050 = dataTrain()
	data_train_100 = dataTrain()
	data_train_150 = dataTrain()
	data_train_200 = dataTrain()
	data_train_250 = dataTrain()
	data_train_300 = dataTrain()

	def __init__(self):
		rospy.init_node('cpm_emg_rec_6', anonymous=True)
		rospy.Subscriber("/joy", Joy, self.cbGetJoy)
		rospy.Subscriber("/joint_1_controller/state", JointState , self.cbJoinStateM1)
		rospy.Subscriber("/Fx_ma", Float64 , self.cbForceMA)
		self.pubM1 = rospy.Publisher("/joint_1_controller/command", Float64 , queue_size=10)

		rospy.Subscriber("/myo_rec/rms_050", Float64MultiArray , self.cbEMGrms050)
		rospy.Subscriber("/myo_rec/mva_050", Float64MultiArray , self.cbEMGmva050)
		rospy.Subscriber("/myo_rec/rms_100", Float64MultiArray , self.cbEMGrms100)
		rospy.Subscriber("/myo_rec/mva_100", Float64MultiArray , self.cbEMGmva100)
		rospy.Subscriber("/myo_rec/rms_150", Float64MultiArray , self.cbEMGrms150)
		rospy.Subscriber("/myo_rec/mva_150", Float64MultiArray , self.cbEMGmva150)
		rospy.Subscriber("/myo_rec/rms_200", Float64MultiArray , self.cbEMGrms200)
		rospy.Subscriber("/myo_rec/mva_200", Float64MultiArray , self.cbEMGmva200)
		rospy.Subscriber("/myo_rec/rms_250", Float64MultiArray , self.cbEMGrms250)
		rospy.Subscriber("/myo_rec/mva_250", Float64MultiArray , self.cbEMGmva250)
		rospy.Subscriber("/myo_rec/rms_300", Float64MultiArray , self.cbEMGrms300)
		rospy.Subscriber("/myo_rec/mva_300", Float64MultiArray , self.cbEMGmva300)
		self.pub_data_train_050 = rospy.Publisher("/data_train_050", dataTrain , queue_size=10)
		self.pub_data_train_100 = rospy.Publisher("/data_train_100", dataTrain , queue_size=10)
		self.pub_data_train_150 = rospy.Publisher("/data_train_150", dataTrain , queue_size=10)
		self.pub_data_train_200 = rospy.Publisher("/data_train_200", dataTrain , queue_size=10)
		self.pub_data_train_250 = rospy.Publisher("/data_train_250", dataTrain , queue_size=10)
		self.pub_data_train_300 = rospy.Publisher("/data_train_300", dataTrain , queue_size=10)

		self.str_ = "start"
		rospy.loginfo(self.str_)

	def cbJoinStateM1(self,data):
		self.current_pos = data.current_pos
		posi_ = 90-m.degrees(self.current_pos)
		self.data_train_050.degree_dxl = posi_
		self.data_train_100.degree_dxl = posi_
		self.data_train_150.degree_dxl = posi_
		self.data_train_200.degree_dxl = posi_
		self.data_train_250.degree_dxl = posi_
		self.data_train_300.degree_dxl = posi_
		# stop at target 
		if abs(self.target-self.current_pos) <= 0.01: 
			self.status_moving = False
			self.pubspeed_M1(0.0)

	def cbGetJoy(self,data):
		self.data_joy = data
		# if self.data_joy.buttons[0] == 1:
		# 	self.target = 1.62316 #1.5708 # 93 degree
		# 	self.publish_M1()
		if self.data_joy.buttons[1] == 1:
			self.target = -0.0872665 # -5 degree
			# self.round = self.round + 1
			self.publish_M1()
		# elif self.data_joy.buttons[3] == 1:
		# 	self.target = 0.0
		# 	self.publish_M1()
		elif self.data_joy.buttons[2] == 1:
			self.target = 1.5708
			self.publish_M1()
		elif self.data_joy.buttons[4] == 1:
			self.pubspeed_M1(0.0)

	def cbForceMA(self,data):
		if self.status_moving:		
			if m.degrees(self.current_pos) >= 0.0 and m.degrees(self.current_pos) <= 91.0:
				fx_get = data.data - 4 - (self.mass_handle * (m.cos((m.pi/2)-self.current_pos)-1))
				self.data_train_050.status_dxl = 1
				self.data_train_050.fx_raw = fx_get
				self.pub_data_train_050.publish(self.data_train_050)
				self.data_train_100.status_dxl = 1
				self.data_train_100.fx_raw = fx_get
				self.pub_data_train_100.publish(self.data_train_100)
				self.data_train_150.status_dxl = 1
				self.data_train_150.fx_raw = fx_get
				self.pub_data_train_150.publish(self.data_train_150)
				self.data_train_200.status_dxl = 1
				self.data_train_200.fx_raw = fx_get
				self.pub_data_train_200.publish(self.data_train_200)
				self.data_train_250.status_dxl = 1
				self.data_train_250.fx_raw = fx_get
				self.pub_data_train_250.publish(self.data_train_250)
				self.data_train_300.status_dxl = 1
				self.data_train_300.fx_raw = fx_get
				self.pub_data_train_300.publish(self.data_train_300)
			self.round_check = 1
		else:
			if self.round_check == 1:
				self.round = self.round + 1
				self.round_check = 0
	
	# publish function
	def pubspeed_M1(self,data):
		self.data_speed.data = data
		self.pubM1.publish(self.data_speed)
	
	def publish_M1(self):
		# check CW by target - current position 
		if self.target-self.current_pos > 0:
			self.pubspeed_M1(self.dxl_speed)
			self.status_moving = False
		elif self.target-self.current_pos < 0:
			self.pubspeed_M1(-self.dxl_speed)
			self.status_moving = True
			# self.round = self.round + 1

	def cbEMGrms050(self,data):
		self.data_train_050.rms_emg_1 = data.data[0]
		self.data_train_050.rms_emg_2 = data.data[1]
		self.data_train_050.rms_emg_3 = data.data[2]
		self.data_train_050.rms_emg_4 = data.data[3]
		self.data_train_050.rms_emg_5 = data.data[4]
		self.data_train_050.rms_emg_6 = data.data[5]
		self.data_train_050.rms_emg_7 = data.data[6]
		self.data_train_050.rms_emg_8 = data.data[7]

		str_ = (">>> Running >>>> round {:2} ".format(self.round))
		rospy.loginfo(str_)
	def cbEMGrms100(self,data):
		self.data_train_100.rms_emg_1 = data.data[0]
		self.data_train_100.rms_emg_2 = data.data[1]
		self.data_train_100.rms_emg_3 = data.data[2]
		self.data_train_100.rms_emg_4 = data.data[3]
		self.data_train_100.rms_emg_5 = data.data[4]
		self.data_train_100.rms_emg_6 = data.data[5]
		self.data_train_100.rms_emg_7 = data.data[6]
		self.data_train_100.rms_emg_8 = data.data[7]
	def cbEMGrms150(self,data):
		self.data_train_150.rms_emg_1 = data.data[0]
		self.data_train_150.rms_emg_2 = data.data[1]
		self.data_train_150.rms_emg_3 = data.data[2]
		self.data_train_150.rms_emg_4 = data.data[3]
		self.data_train_150.rms_emg_5 = data.data[4]
		self.data_train_150.rms_emg_6 = data.data[5]
		self.data_train_150.rms_emg_7 = data.data[6]
		self.data_train_150.rms_emg_8 = data.data[7]
	def cbEMGrms200(self,data):
		self.data_train_200.rms_emg_1 = data.data[0]
		self.data_train_200.rms_emg_2 = data.data[1]
		self.data_train_200.rms_emg_3 = data.data[2]
		self.data_train_200.rms_emg_4 = data.data[3]
		self.data_train_200.rms_emg_5 = data.data[4]
		self.data_train_200.rms_emg_6 = data.data[5]
		self.data_train_200.rms_emg_7 = data.data[6]
		self.data_train_200.rms_emg_8 = data.data[7]
	def cbEMGrms250(self,data):
		self.data_train_250.rms_emg_1 = data.data[0]
		self.data_train_250.rms_emg_2 = data.data[1]
		self.data_train_250.rms_emg_3 = data.data[2]
		self.data_train_250.rms_emg_4 = data.data[3]
		self.data_train_250.rms_emg_5 = data.data[4]
		self.data_train_250.rms_emg_6 = data.data[5]
		self.data_train_250.rms_emg_7 = data.data[6]
		self.data_train_250.rms_emg_8 = data.data[7]
	def cbEMGrms300(self,data):
		self.data_train_300.rms_emg_1 = data.data[0]
		self.data_train_300.rms_emg_2 = data.data[1]
		self.data_train_300.rms_emg_3 = data.data[2]
		self.data_train_300.rms_emg_4 = data.data[3]
		self.data_train_300.rms_emg_5 = data.data[4]
		self.data_train_300.rms_emg_6 = data.data[5]
		self.data_train_300.rms_emg_7 = data.data[6]
		self.data_train_300.rms_emg_8 = data.data[7]
	
	def cbEMGmva050(self,data):
		self.data_train_050.mva_emg_1 = data.data[0]
		self.data_train_050.mva_emg_2 = data.data[1]
		self.data_train_050.mva_emg_3 = data.data[2]
		self.data_train_050.mva_emg_4 = data.data[3]
		self.data_train_050.mva_emg_5 = data.data[4]
		self.data_train_050.mva_emg_6 = data.data[5]
		self.data_train_050.mva_emg_7 = data.data[6]
		self.data_train_050.mva_emg_8 = data.data[7]
	def cbEMGmva100(self,data):
		self.data_train_100.mva_emg_1 = data.data[0]
		self.data_train_100.mva_emg_2 = data.data[1]
		self.data_train_100.mva_emg_3 = data.data[2]
		self.data_train_100.mva_emg_4 = data.data[3]
		self.data_train_100.mva_emg_5 = data.data[4]
		self.data_train_100.mva_emg_6 = data.data[5]
		self.data_train_100.mva_emg_7 = data.data[6]
		self.data_train_100.mva_emg_8 = data.data[7]
	def cbEMGmva150(self,data):
		self.data_train_150.mva_emg_1 = data.data[0]
		self.data_train_150.mva_emg_2 = data.data[1]
		self.data_train_150.mva_emg_3 = data.data[2]
		self.data_train_150.mva_emg_4 = data.data[3]
		self.data_train_150.mva_emg_5 = data.data[4]
		self.data_train_150.mva_emg_6 = data.data[5]
		self.data_train_150.mva_emg_7 = data.data[6]
		self.data_train_150.mva_emg_8 = data.data[7]
	def cbEMGmva200(self,data):
		self.data_train_200.mva_emg_1 = data.data[0]
		self.data_train_200.mva_emg_2 = data.data[1]
		self.data_train_200.mva_emg_3 = data.data[2]
		self.data_train_200.mva_emg_4 = data.data[3]
		self.data_train_200.mva_emg_5 = data.data[4]
		self.data_train_200.mva_emg_6 = data.data[5]
		self.data_train_200.mva_emg_7 = data.data[6]
		self.data_train_200.mva_emg_8 = data.data[7]
	def cbEMGmva250(self,data):
		self.data_train_250.mva_emg_1 = data.data[0]
		self.data_train_250.mva_emg_2 = data.data[1]
		self.data_train_250.mva_emg_3 = data.data[2]
		self.data_train_250.mva_emg_4 = data.data[3]
		self.data_train_250.mva_emg_5 = data.data[4]
		self.data_train_250.mva_emg_6 = data.data[5]
		self.data_train_250.mva_emg_7 = data.data[6]
		self.data_train_250.mva_emg_8 = data.data[7]
	def cbEMGmva300(self,data):
		self.data_train_300.mva_emg_1 = data.data[0]
		self.data_train_300.mva_emg_2 = data.data[1]
		self.data_train_300.mva_emg_3 = data.data[2]
		self.data_train_300.mva_emg_4 = data.data[3]
		self.data_train_300.mva_emg_5 = data.data[4]
		self.data_train_300.mva_emg_6 = data.data[5]
		self.data_train_300.mva_emg_7 = data.data[6]
		self.data_train_300.mva_emg_8 = data.data[7]
	
	
if __name__ == "__main__":
	try:
		run = cpm_emg()
		rospy.spin()
	except rospy.ROSInterruptException:
		pass