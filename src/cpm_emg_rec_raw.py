#!/usr/bin/env python
'''
	Position and maybe Force-Velocity control in CPM mode.
	Control a robot for recording raw data samples of EMG, Theta and Force.
	setup Dynamixel in wheel mode.

	readme file name "step emg cpm"

	start: 30/09/2020
	*Note: Just edit Topic Name of Sub and Pub

'''

import rospy
from std_msgs.msg import Float64MultiArray
from ros_myo.msg import EmgArray
from dynamixel_msgs.msg import JointState
from std_msgs.msg import Float64
from sensor_msgs.msg import Joy
from dxl_test_msg.msg import dataRaw
from geometry_msgs.msg import WrenchStamped
import math as m 

class cpm_emg:
	data_joy = Joy()
	target = 0.0 #radius
	dxl_speed = 0.40 # rad/s
	current_pos = 0.0
	data_speed = Float64()

	status_moving = False
	round = 0
	round_check = 0

	sampleCheck = 0
	sampleDelay = 0
	sampleCount = 0
	
	mass_zero = 8 # edit N
	mass_handle = 0.8
	str_ = ""

	force_x = Float64()

	data_raw = dataRaw()

	def __init__(self):
		rospy.init_node('cpm_emg_rec_raw', anonymous=True)
		rospy.Subscriber("/joy", Joy, self.cbGetJoy)
		rospy.Subscriber("/joint_1_controller/state", JointState , self.cbJoinStateM1)
		rospy.Subscriber("/Fx_ma", Float64 , self.cbForceMA)
		rospy.Subscriber("/myo_raw/myo_emg", EmgArray , self.cbEMGraw)   

		self.pubM1 = rospy.Publisher("/joint_1_controller/command", Float64 , queue_size=10) 
		self.pub_data_raw = rospy.Publisher("/data_raw", dataRaw , queue_size=10)

		self.str_ = "start"
		rospy.loginfo(self.str_)

	def cbJoinStateM1(self,data):
		self.current_pos = data.current_pos
		posi_ = 90-m.degrees(self.current_pos)
		self.data_raw.degree_dxl = posi_
		self.data_raw.radian_dxl = self.current_pos
		# self.data_raw.v_const = self.dxl_speed
		# self.data_raw.v_feed = 
		# stop at target 
		if abs(self.target-self.current_pos) <= 0.01: 
			self.status_moving = False
			self.pubspeed_M1(0.0)

	def cbForceMA(self,data):
		# fx_get = data.data - 4 - (self.mass_handle * (m.cos((m.pi/2)-self.current_pos)-1))
		self.data_raw.fx_raw  = data.data
		self.data_raw.fx_comp = data.data - (self.mass_handle * (m.cos((m.pi/2)-self.current_pos)-1))

		if self.status_moving:		
			if m.degrees(self.current_pos) >= 0.0 and m.degrees(self.current_pos) <= 91.0:
				self.data_raw.status_dxl = 1  # up
				#self.pub_data_raw.publish(self.data_raw)
			elif m.degrees(self.current_pos) > 91.0:
				self.data_raw.status_dxl = 0  # down
				#self.pub_data_raw.publish(self.data_raw)
			self.round_check = 1
		else:
			if self.round_check == 1:
				self.round = self.round + 1
				self.round_check = 0
			self.data_raw.status_dxl = 0
		
		self.pub_data_raw.publish(self.data_raw)
	
	def cbEMGraw(self,data):
		self.data_raw.emg_raw = data.data
		# self.pub_data_raw.publish(self.data_raw)
		"""
		if self.sampleCheck == 1:
			self.pub_data_raw.publish(self.data_raw)
			self.sampleCount = self.sampleCount + 1
			if self.sampleCount >= 300:
				self.sampleCount = 0
				self.sampleCheck = 0
				self.sampleDelay = 1
		
		if self.sampleDelay == 1:
			self.publish_M1()
			self.sampleDelay = 0
		"""

		str_ = (">>> Running >>>> round {:2} >>> {:2} ".format(self.round,self.current_pos))
		rospy.loginfo(str_)
	
	def cbGetJoy(self,data):
		self.data_joy = data
		if self.data_joy.buttons[1] == 1:
			self.target = -0.0872665 # -5 degree
			# self.sampleCheck = 1
			self.publish_M1()
		elif self.data_joy.buttons[2] == 1:
			self.target = 1.5708
			self.publish_M1()
		elif self.data_joy.buttons[4] == 1:
			self.pubspeed_M1(0.0)
	
	# publish function
	def pubspeed_M1(self,data):
		self.data_speed.data = data
		self.pubM1.publish(self.data_speed)
	
	def publish_M1(self):
		# check CW by target - current position 
		if self.target-self.current_pos > 0:
			self.pubspeed_M1(self.dxl_speed)
			self.status_moving = False
		elif self.target-self.current_pos < 0:
			self.pubspeed_M1(-self.dxl_speed)
			self.status_moving = True
		
	
if __name__ == "__main__":
	try:
		run = cpm_emg()
		rospy.spin()
	except rospy.ROSInterruptException:
		pass