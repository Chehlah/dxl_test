#!/usr/bin/env python
'''
	Vector control at end-effector
	2DOF Robot

	set dynamixel in wheel mode
	!!! be carefull !!!

	Setting Day 04/08/2019
	ID 15,12 J1,J2

	Project: EP-0408-02

'''
import rospy
from dynamixel_msgs.msg import JointState
from std_msgs.msg import Float64
from std_msgs.msg import Int8
from sensor_msgs.msg import Joy
from geometry_msgs.msg import PointStamped
from geometry_msgs.msg import Point
from geometry_msgs.msg import WrenchStamped

import math as m

class vector2dof:
	# Attributes
	# Force sensor
	data_ft = WrenchStamped()
	# Jacobian Parameters
	jM1 = 0.002
	jM2 = 0.002
	omega1 = 0.0
	omega2 = 0.0
	L1 = 0.4 		# m
	L2 = 0.4
	x = 0.0
	z = 0.0
	Vx = 0.0 		# m/s
	Vz = 0.0
	Vconst = 0.05   # m/s

	# sub and pub
	getJoy = Joy()
	data1 = Float64()
	data2 = Float64()
	
	# low pass filter param
	Xold_joint1 = 0.0
	Xold_joint2 = 0.0
	lpf_case1 = 1
	lpf_case2 = 1
	data_out_M1 = 0.0
	data_out_M2 = 0.0
	min_djoint = 0.1   # the value 0.02 is max range the data change in normal

	# vector control
	target = {'x':1.0,'z':1.0}
	d_case = 0
	min_dpoint = 0.003 # 0.001 m
	pointS = PointStamped()
	pointXYZ = Point()
	Vtheta = 0.0

	# circle control
	circle_point = { 'x_o':0.75,'z_o':0.75,'R':0.25,'S':0.0,'x_p':0.75,'z_p':0.75}
	c_case = 0 
	t_case = 0 # translation case

	# force velocity control
	fv_case = 0
	fx,fy,f = 0.0,0.0,0.0
	f_theta,f_Vtheta = 0.0,0.0

	def __init__(self):
		rospy.init_node('vector_2dof', anonymous=True)
		self.subFT = rospy.Subscriber("/ft_sensor/netft_data", WrenchStamped , self.cbFTsensor)
		self.subM1 = rospy.Subscriber("/wheel_1_controller/state", JointState , self.cbWheelStateM1)
		self.subM2 = rospy.Subscriber("/wheel_2_controller/state", JointState , self.cbWheelStateM2)
		self.subJoy = rospy.Subscriber("/joy", Joy, self.cbGetJoy)
		self.subTarget = rospy.Subscriber("/target", Int8, self.cbTarget)
		self.pubM1speed = rospy.Publisher("/wheel_1_controller/command", Float64 , queue_size=10)
		self.pubM2speed = rospy.Publisher("/wheel_2_controller/command", Float64 , queue_size=10)
		self.pubPointStamped = rospy.Publisher("/pose/point",PointStamped,queue_size=10 )
		self.pubPoint = rospy.Publisher("/pointXYZ",Point,queue_size=10 )
		# pointStamped frame ID
		self.pointS.header.frame_id = 'eff'
	
	# Method
	# Subscriber
	def cbTarget(self,data):
		if data.data == 1: # linear (point to point) control
			self.d_case = 1	
			self.target['x'] = 0.2
			self.target['z'] = 0.56
		elif data.data == 2:
			self.d_case = 1	
			self.target['x'] = 0.56
			self.target['z'] = 0.2
		elif data.data == 3: # circle control 
			self.c_case = 1 
			self.t_case = 1	
			self.target['x'] = 0.75
			self.target['z'] = 0.75
		elif data.data == 4: # test frame transform function 
			self.fv_case = 1
			self.target['x'] = 0.50
			self.target['z'] = 0.75
		elif data.data == 5:
			self.fv_case = 2
			self.target['x'] = 1.0
			self.target['z'] = 0.75
		else:
			self.d_case = 0
			self.c_case = 0
			self.fv_case = 0
			self.Vx = 0.0
			self.Vz = 0.0

	def cbGetJoy(self,data):
		self.getJoy = data
		if self.getJoy.axes[0] == 1.0 and self.getJoy.axes[1] == 0.0:
			self.Vx = self.Vconst
		elif self.getJoy.axes[0] == -1.0 and self.getJoy.axes[1] == 0.0:
			self.Vx = -self.Vconst
		elif self.getJoy.axes[0] == 0.0 and self.getJoy.axes[1] == 1.0:
			self.Vz = self.Vconst
		elif self.getJoy.axes[0] == 0.0 and self.getJoy.axes[1] == -1.0:
			self.Vz = -self.Vconst
		elif self.getJoy.buttons[1] == 1:
			self.i_case = 1	
			self.target['x'] = 1.0
			self.target['z'] = 1.0
		elif self.getJoy.buttons[0] == 1:
			self.i_case = 1	
			self.target['x'] = 0.5
			self.target['z'] = 0.5
		elif self.getJoy.buttons[2] == 1:
			self.i_case = 0
			self.Vx = 0.0
			self.Vz = 0.0
		else:
			self.Vx = 0.0
			self.Vz = 0.0

	def cbFTsensor(self,data):
		self.data_ft = data
		self.fx = data.wrench.force.x
		self.fy = data.wrench.force.y

	def cbWheelStateM1(self,data):
		self.jM1 = data.current_pos
		if self.lpf_case1 == 1:
			self.Xold_joint1 = self.jM1
			self.lpf_case1 = 0
		if abs(self.Xold_joint1 - self.jM1) < self.min_djoint:
			self.data_out_M1 = self.jM1
		else:
			self.data_out_M1 = self.Xold_joint1
		self.Xold_joint1 = self.data_out_M1
		self.jM1 = self.data_out_M1

	def cbWheelStateM2(self,data):
		self.jM2 = data.current_pos
		if self.lpf_case2 == 1:
			self.Xold_joint2 = self.jM2
			self.lpf_case2 = 0
		if abs(self.Xold_joint2 - self.jM2) < self.min_djoint: 
			self.data_out_M2 = self.jM2
		else:
			self.data_out_M2 = self.Xold_joint2
		self.Xold_joint2 = self.data_out_M2
		self.jM2 = self.data_out_M2
		# run main
		self.main_run()
	
	# Publisher
	def publishSpeed(self):
		self.data1.data = self.omega1
		self.pubM1speed.publish(self.data1)
		self.data2.data = self.omega2
		self.pubM2speed.publish(self.data2)

	def pointStamped(self):
		self.pointS.point.x = self.x
		self.pointS.point.y = 0.0
		self.pointS.point.z = self.z
		self.pointXYZ.x = self.x
		self.pointXYZ.y = 0.0
		self.pointXYZ.z = self.z
		self.pubPointStamped.publish(self.pointS)
		self.pubPoint.publish(self.pointXYZ)

	# Math Calculation
	def forwardKinematic(self):
		self.z = (self.L1*m.cos(self.jM1))+(self.L2*m.cos(self.jM1+self.jM2))
		self.x = (self.L1*m.sin(self.jM1))+(self.L2*m.sin(self.jM1+self.jM2))

	def inverseJacobian(self):
		self.omega1 = (((m.cos(self.jM1+self.jM2)*self.Vz)+(m.sin(self.jM1+self.jM2)*self.Vx))
			/(m.sin(self.jM2)*self.L1))
		self.omega2 = ((((self.L1*m.cos(self.jM1)+self.L2*m.cos(self.jM1+self.jM2))*self.Vz)
			+(self.Vx*(self.L1*m.sin(self.jM1)+self.L2*m.sin(self.jM1+self.jM2))))
			/(-(self.L1*self.L2*m.sin(self.jM2))))
	
	# Controller Function
	def direction_vector(self):
		x = self.target['x'] - self.x
		z = self.target['z'] - self.z
		if abs(x) <= self.min_dpoint and abs(z) <= self.min_dpoint:
			self.d_case = 0
			self.Vx = 0.0
			self.Vz = 0.0
		else:
			self.Vtheta = m.atan2(x,z)					#(z,x)
			self.Vx = self.Vconst * m.sin(self.Vtheta)	#cos
			self.Vz = self.Vconst * m.cos(self.Vtheta)	#sin

	def circle_control(self):
		x = 0.0
		z = 0.0
		if self.t_case == 1:
			x = self.target['x'] - self.x
			z = self.target['z'] - self.z
		elif self.t_case == 2:
			self.circle_point['x_p'] = (self.circle_point['R']*m.cos(m.radians(self.circle_point['S'])))+self.circle_point['x_o']
			self.circle_point['z_p'] = (self.circle_point['R']*m.sin(m.radians(self.circle_point['S'])))+self.circle_point['z_o']
			x = self.circle_point['x_p'] - self.x
			z = self.circle_point['z_p'] - self.z

		if abs(x) <= self.min_dpoint and abs(z) <= self.min_dpoint:
			if self.t_case == 1:
				self.Vx = 0.0
				self.Vz = 0.0
				self.t_case = 2
			elif self.t_case == 2:
				self.circle_point['S'] = self.circle_point['S'] + 5.0 # 1.0
				if self.circle_point['S'] > 90:
					self.t_case = 3
					self.Vx = 0.0
					self.Vz = 0.0
			elif self.t_case == 3:
				self.Vx = 0.0
				self.Vz = 0.0
		else:
			Vtheta = m.atan2(z,x)
			self.Vx = self.Vconst * m.cos(Vtheta)
			self.Vz = self.Vconst * m.sin(Vtheta)
	
	def force_direction(self): # transform sensor frame to velocity frame
		self.f = m.sqrt(m.pow(self.fx,2)+m.pow(self.fy,2))
		f_theta = m.atan2(self.fy,self.fx)
		self.f_Vtheta = m.degrees(f_theta) + 90 + m.degrees(self.jM1+self.jM2) # real use 
		self.f_theta = m.degrees(self.Vtheta) - 90 - m.degrees(self.jM1+self.jM2)

	def force_velocity_control(self):
		self.force_direction()
		# demo force control
		'''
			if f_Vtheta == s_vx and f > f_hold
				move V_const to point target 
		'''
	# Main
	def main_run(self):
		self.forwardKinematic()
		if self.d_case:
			self.direction_vector()
		elif self.c_case:
			self.circle_control()
		elif self.fv_case == 1 or self.fv_case == 2:
			self.direction_vector()
			self.force_velocity_control()
		self.inverseJacobian()
		self.publishSpeed()
		# str_ = ("c_case {} t_case {} x {: 3.3f} and z {: 3.3f} also S {: 3.3f}".format(
		# 	self.c_case,self.t_case,self.x,self.z,self.circle_point['S']))
		str_ = ("fv_case {} x{: 3.3f} z{: 3.3f} J1{: 3.3f} J2{: 3.3f} F{: 3.3f} S_F{: 3.3f} S_V{: 3.3f}".format(
			self.fv_case,self.x,self.z,m.degrees(self.jM1),m.degrees(self.jM2),self.f,self.f_theta,m.degrees(self.Vtheta)))
		rospy.loginfo(str_)
		self.pointStamped()


def main():
	run_vector = vector2dof()
	rospy.spin()

if __name__ == '__main__':
	try:
		main()
	except rospy.ROSInterruptException:
		pass