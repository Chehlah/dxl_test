#!/usr/bin/env python
'''
	input
	test topic flow 
'''
import rospy, os, sys
from std_msgs.msg import String

class strIn:
	j = 0
	str_ = String()

	def __init__(self):
		rospy.init_node('in_hzControl', anonymous=True)
		self.pubStr = rospy.Publisher("/listener", String, queue_size=1)

	def pubStrIn(self):
		self.j = self.j + 1
		sstr = ">>>> {}".format(self.j)
		self.str_.data = sstr
		rospy.loginfo(self.str_.data)
		self.pubStr.publish(self.str_)

def main(args):
	# init class here
	sIn = strIn()
	rate = rospy.Rate(20) # Hz
	while not rospy.is_shutdown():
		sIn.pubStrIn()
		rate.sleep()
	# rospy.spin()   

if __name__ == "__main__":
	try:
		main(sys.argv)
	except rospy.ROSInterruptException:
		pass