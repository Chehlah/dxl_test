#!/urs/bin/env python
'''
	Write google speech recognition in OOP
	1. publish Class
	2. speech recognition test 
'''

import rospy, os, sys
import speech_recognition as sr
from std_msgs.msg import String
import xml.etree.ElementTree as ET


class gspeech:
	# Attributes
	strPub = String()
	
	rRec = sr.Recognizer()
	rMic = sr.Microphone()
	time_threshold = 0.5
	duration = 1
	device_index = 0
	j = 0

	def __init__(self):
		rospy.init_node('speech_google', anonymous=True)
		self.pubTar = rospy.Publisher("/target", String, queue_size=10)

		self.rRec.pause_threshold = self.time_threshold
		
		for index, name in enumerate(self.rMic.list_microphone_names()):
			print ("Microphone with name \"{1}\" found for `Microphone(device_index={0})`".format(index, name))
			if name == "default":
				self.device_index = index # select device		
		
		with sr.Microphone(self.device_index) as source: # rMic ??
			self.rRec.adjust_for_ambient_noise(source,self.duration)

	# Method
	def pubTarget(self):
		# self.strPub.data = "GO GO"
		self.pubTar.publish(self.strPub)

	def speechGoogle(self):
		self.j = self.j + 1
		with sr.Microphone(self.device_index) as source: # rMic ??
			print ("Robot is listening now")
			audio = self.rRec.listen(source) 
		try:
			# recognize speech using Google Speech Recognition
			print("Waiting the recognize from Google")
			speech = self.rRec.recognize_google(audio,key = None, language = "en-US", show_all = False)
			self.strPub.data = speech
			self.pubTarget()
		except sr.UnknownValueError:
			print(">> {} << Google Speech Recognition could not understand audio".format(self.j))
		except sr.RequestError as e:
			print("Could not request results from Google Speech Recognition service; {0}".format(e))

def main(args):
	# init class here
	gsp = gspeech()
	
	rate = rospy.Rate(10) # Hz
	while not rospy.is_shutdown():
		# gsp.pubTarget()
		# main run here
		gsp.speechGoogle()
		# rospy.sleep(1.0)
		rate.sleep()
	# rospy.spin()   

if __name__ == "__main__":
	try:
		main(sys.argv)
	except rospy.ROSInterruptException:
		pass