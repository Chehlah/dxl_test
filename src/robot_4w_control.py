#!/usr/bin/env python

from distutils.command.config import config

import rospy
from std_msgs.msg import Float64MultiArray
from dynamixel_msgs.msg import JointState
from dynamixel_msgs.msg import MotorStateList
from sensor_msgs.msg import Joy
from std_msgs.msg import Float64
from geometry_msgs.msg import Wrench
import math as m 
import numpy as np

from dynamic_reconfigure.server import Server
from dxl_test.cfg import configConfig

class run_4w:

	passive_on = 1

	speed_pub_11 = Float64()
	speed_pub_12 = Float64()
	speed_pub_13 = Float64()
	speed_pub_14 = Float64()

	position = Float64MultiArray()
	tpass = 0.0
	speed_fb = []

	speed = 0.0    # m/s
	direction = 0  # degrees
	speed_up = 0.0
	omega = 0
	omega_up = 0.0
	get_force = []

	slope = 0.005  # of Velocity ()
	threshold = 2.0 # of Force
	threshold_2 = 0.1
	threshold_z = 0.3
	threshold_z_2 = 0.03
	z_offset = 10
	double_threshold = 0

	str_ = "Start"

	def __init__(self):
		rospy.init_node('besic_control_4w', anonymous=True)
		
		rospy.Subscriber("/joy", Joy, self.cbGetJoy)
		rospy.Subscriber("/wheel_11_controller/state",JointState,self.cbWheel_11)
		rospy.Subscriber("/wheel_12_controller/state",JointState,self.cbWheel_12)
		rospy.Subscriber("/wheel_13_controller/state",JointState,self.cbWheel_13)
		rospy.Subscriber("/wheel_14_controller/state",JointState,self.cbWheel_14)
		# rospy.Subscriber("/fx_ma", Float64 , self.cbFTsensor)
		rospy.Subscriber("/fxyz_ma", Wrench , self.cbFTsensor)

		self.wheel_11 = rospy.Publisher("/wheel_11_controller/command", Float64 , queue_size=10)
		self.wheel_12 = rospy.Publisher("/wheel_12_controller/command", Float64 , queue_size=10)
		self.wheel_13 = rospy.Publisher("/wheel_13_controller/command", Float64 , queue_size=10)
		self.wheel_14 = rospy.Publisher("/wheel_14_controller/command", Float64 , queue_size=10)
		self.pub_position = rospy.Publisher("/pub/position", Float64MultiArray , queue_size=10)

		rospy.loginfo(self.str_)
		self.position.data = [0.0]*3
		self.tpass = rospy.get_time()
		self.speed_fb = [0.0]*4
		self.get_force = [0.0]*3
		rospy.loginfo(self.tpass)

		srv = Server(configConfig, self.callback)

	def callback(self, config, level):
		rospy.loginfo("""Reconfigure Request: {Friction_Gain} , {Linear_Threshold} , {Rotation_Threshold} ,{Z_offset} , {Mode_Select}""".format(**config))
		self.slope       = config.Friction_Gain     # of Velocity ()
		self.threshold   = config.Linear_Threshold # of Force
		self.threshold_z = config.Rotation_Threshold
		self.z_offset    = config.Z_offset
		self.passive_on  = config.Mode_Select
		return config

	def cbFTsensor(self,data):
		
		if self.passive_on == 1:

			Fxy = np.sqrt(m.pow(data.force.x,2)+m.pow(data.force.y,2))
			if self.double_threshold == 0: # off
				if  Fxy >= self.threshold or abs(data.torque.z) >= self.threshold_z:
					# X axis to Y axis and Y axis to -X axis
					self.linear_control_2( data.force.y, -data.force.x, data.torque.z , self.slope*self.z_offset)
					self.double_threshold = 1
			elif self.double_threshold == 1:
				if  Fxy >= self.threshold_2 or abs(data.torque.z) >= self.threshold_z_2:
					# X axis to Y axis and Y axis to -X axis
					self.linear_control_2( data.force.y, -data.force.x, data.torque.z , self.slope*self.z_offset)
				else:
					self.double_threshold = 0
					self.linear_control_2(0.0,0.0,0.0,0.0)

		# Fxy > 2.0 N and direction of force
		# Fxy = np.sqrt(m.pow(data.force.x,2)+m.pow(data.force.y,2))
		# # abs(data.force.x) >= self.threshold or abs(data.force.y) >= self.threshold
		# if  Fxy >= self.threshold or abs(data.torque.z) >= self.threshold_z:
		# 	# X axis to Y axis and Y axis to -X axis
		# 	x = data.force.y-self.threshold if data.force.y >= 0 else data.force.y+self.threshold  
		# 	y = -data.force.x-self.threshold if data.force.x <= 0 else -data.force.x+self.threshold
		# 	z = data.torque.z-self.threshold_z if data.torque.z >= 0 else data.torque.z+self.threshold_z
		# 	self.linear_control_2( x, y, z , self.slope*self.z_offset*10)
		# # elif data.torque.z >= self.threshold_z or data.torque.z <= -self.threshold_z:
		# # 	self.rotation_control(self.get_force[2]*self.z_offset,1)
		# else:
		# 	self.linear_control_2(0.0,0.0,0.0,0.0)

		# >>> mono Axis Control <<< # 
		# if self.get_force[0] > 2.0 or self.get_force[0] < -2.0:
		# 	# get speed and direction 
		# 	if get_force > 0:
		# 		self.speed = 0.01
		# 		self.direction = 90
		# 	elif get_force < 0:
		# 		self.speed = 0.01
		# 		self.direction = -90
		# else:
		# 	self.speed = 0.0
		# 	self.direction = 0
		# 	self.omega = 0
		
		# if self.speed == 0.01 :
		# 	self.linear_control(self.speed_up,self.direction)
		# elif self.omega != 0:
		# 	self.rotation_control(self.omega_up,self.omega)
		# else:
		# 	self.linear_control(self.speed,self.direction)
		#   self.rotation_control(self.omega_up,self.omega)


	def cbGetJoy(self,data):
		# pass
		if data.buttons[6] == 1:
			if self.passive_on == 2:
				self.passive_on = 1
			else:
				self.passive_on = 2

		if self.passive_on == 2:

			# >>> Joy XY controller <<< #
			if    data.axes[1] ==  1.0:  # +x
				self.speed = 0.01
				self.direction = 0
			elif  data.axes[1] == -1.0:  # -x
				self.speed = 0.01
				self.direction = 180
			elif  data.axes[0] ==  1.0:  # +y
				self.speed = 0.01
				self.direction = 90
			elif  data.axes[0] == -1.0:  # -y
				self.speed = 0.01
				self.direction = -90

			# >>> Joy Rotation controller <<< #
			elif  data.buttons[1] == 1:  # CCW
				self.omega =  -1
			elif  data.buttons[3] == 1:  # CW
				self.omega =  1

			# elif  data.buttons[7] == 1:  # speed up
			# 	if self.speed_up < 0.5:
			# 		self.speed_up = self.speed_up + 0.01
			# 		self.omega_up = self.omega_up + 0.03
			# 		self.str_ = "speed_up {} omega_up {}".format(self.speed_up,self.omega_up)
			# 		rospy.loginfo(self.str_)
			# 		self.omega_up = self.omega_up - 0.03
			# 		self.str_ = "speed_up {} omega_up {}".format(self.speed_up,self.omega_up)
			# 		rospy.loginfo(self.str_)

			else:
				self.speed = 0.0
				self.direction = 0
				self.omega = 0
			
			# >>> Joy Speed XY controller <<< #
			if  data.buttons[7] == 1:  # speed up
				if self.speed_up < 0.5:
					self.speed_up = self.speed_up + 0.01
					self.omega_up = self.omega_up + 0.05
					self.str_ = "speed_up {} omega_up {}".format(self.speed_up,self.omega_up)
					rospy.loginfo(self.str_)
			elif  data.buttons[5] == 1:
				if self.speed_up > 0.0:  # speed down
					self.speed_up = self.speed_up - 0.01
					self.omega_up = self.omega_up - 0.05
					self.str_ = "speed_up {} omega_up {}".format(self.speed_up,self.omega_up)
					rospy.loginfo(self.str_)

			if self.speed == 0.01 :
				self.linear_control(self.speed_up,self.direction)
			elif self.omega != 0:
				self.rotation_control(self.omega_up,self.omega)
			else:
				self.linear_control(self.speed,self.direction)
				self.rotation_control(self.omega_up,self.omega)

	def linear_control_2(self,fx,fy,omega_up,omega):
		Vx = self.slope * fx 
		Vy = self.slope * fy 
		V11 = (4/0.06) * ( Vx*m.cos(m.radians(45)) - Vy*m.sin(m.radians(45)) )
		V12 = (4/0.06) * ( Vx*m.cos(m.radians(45)) + Vy*m.sin(m.radians(45)) )
		V13 = (4/0.06) * (-Vx*m.cos(m.radians(45)) + Vy*m.sin(m.radians(45)) )
		V14 = (4/0.06) * (-Vx*m.cos(m.radians(45)) - Vy*m.sin(m.radians(45)) )

		V11_ = (4/0.06) * (-0.12 * omega*omega_up)
		V12_ = (4/0.06) * (-0.12 * omega*omega_up)
		V13_ = (4/0.06) * (-0.12 * omega*omega_up)
		V14_ = (4/0.06) * (-0.12 * omega*omega_up)

		self.wheel_pub(V11+V11_,V12+V12_,V13+V13_,V14+V14_)


	def linear_control(self,speed,direction):
		Vx = speed * m.cos(m.radians(direction))
		Vy = speed * m.sin(m.radians(direction))
		V11 = (4/0.06) * ( Vx*m.cos(m.radians(45)) - Vy*m.sin(m.radians(45)) )
		V12 = (4/0.06) * ( Vx*m.cos(m.radians(45)) + Vy*m.sin(m.radians(45)) )
		V13 = (4/0.06) * (-Vx*m.cos(m.radians(45)) + Vy*m.sin(m.radians(45)) )
		V14 = (4/0.06) * (-Vx*m.cos(m.radians(45)) - Vy*m.sin(m.radians(45)) )
		self.wheel_pub(V11,V12,V13,V14)

	def rotation_control(self,omega_up,omega):
		V11 = (4/0.06) * (-0.12 * omega*omega_up)
		V12 = (4/0.06) * (-0.12 * omega*omega_up)
		V13 = (4/0.06) * (-0.12 * omega*omega_up)
		V14 = (4/0.06) * (-0.12 * omega*omega_up)
		self.wheel_pub(V11,V12,V13,V14)

	def wheel_pub(self,V11,V12,V13,V14):
		self.speed_pub_11.data = V11
		self.speed_pub_12.data = V12
		self.speed_pub_13.data = V13
		self.speed_pub_14.data = V14
		self.wheel_11.publish(self.speed_pub_11)
		self.wheel_12.publish(self.speed_pub_12)
		self.wheel_13.publish(self.speed_pub_13)
		self.wheel_14.publish(self.speed_pub_14)

	def cbWheel_11(self,data):
		self.speed_fb[0] = data.velocity
		timenow = rospy.get_time()
		self.position_odometry(timenow,self.tpass)
		self.tpass = timenow

	def cbWheel_12(self,data):
		self.speed_fb[1] = data.velocity

	def cbWheel_13(self,data):
		self.speed_fb[2] = data.velocity

	def cbWheel_14(self,data):
		self.speed_fb[3] = data.velocity

	def position_odometry(self,timenow,timelass):
		Vx = (0.06/4) * (   self.speed_fb[0]*m.cos(m.radians(45)) + self.speed_fb[1]*m.cos(m.radians(45))
			- self.speed_fb[2]*m.cos(m.radians(45)) - self.speed_fb[3]*m.cos(m.radians(45)) ) 
		Vy = (0.06/4) * ( - self.speed_fb[0]*m.sin(m.radians(45)) + self.speed_fb[1]*m.sin(m.radians(45))
			+ self.speed_fb[2]*m.sin(m.radians(45)) - self.speed_fb[3]*m.sin(m.radians(45)) ) 
		self.position.data[0] = Vx * (timenow - timelass)  + self.position.data[0]
		self.position.data[1] = Vy * (timenow - timelass)  + self.position.data[1]
		self.position.data[2] = timenow
		self.pub_position.publish(self.position)
		

if __name__ == "__main__":
	try:
		run = run_4w() # run class
		rospy.spin()
	except rospy.ROSInterruptException:
		pass