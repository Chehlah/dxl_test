#!/usr/bin/env python
# -*- coding: utf-8 -*-

################################################################################
# Copyright 2017 ROBOTIS CO., LTD.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
################################################################################

# Author: Ryu Woon Jung (Leon)

#
# *********     Read and Write Example      *********
#
#
# Available DXL model on this example : All models using Protocol 1.0
# This example is tested with a DXL MX-28, and an USB2DYNAMIXEL
# Be sure that DXL MX properties are already set as %% ID : 1 / Baudnum : 34 (Baudrate : 57600)
#

import os 
import rospy
from sensor_msgs.msg import Joy
from std_msgs.msg import Float32MultiArray
from std_msgs.msg import Float64

if os.name == 'nt':
	import msvcrt
	def getch():
		return msvcrt.getch().decode()
else:
	import sys, tty, termios
	fd = sys.stdin.fileno()
	old_settings = termios.tcgetattr(fd)
	def getch():
		try:
			tty.setraw(sys.stdin.fileno())
			ch = sys.stdin.read(1)
		finally:
			termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
		return ch

from dynamixel_sdk import *                    # Uses Dynamixel SDK library

class dxl_controller():

	output_velocity = Float32MultiArray()
	current_pos     = Float64()
	feedback_speed  = Float64()
	control_effort  = 0.0
	data_joy = Joy()
	
	# Control table address
	ADDR_MX_TORQUE_ENABLE      = 24               # Control table address is different in Dynamixel model
	ADDR_MX_MOVING_SPEED       = 32
	ADDR_MX_PRESENT_POSITION   = 36
	ADDR_MX_PRESENT_SPEED      = 38
	ADDR_MX_P_GAIN             = 28
	ADDR_MX_I_GAIN             = 27

	# Protocol version
	PROTOCOL_VERSION            = 1.0               # See which protocol version is used in the Dynamixel

	# Default setting
	DXL_ID                      = 1                 # Dynamixel ID : 1
	BAUDRATE                    = 1000000           # Dynamixel default baudrate : 57600
	DEVICENAME                  = '/dev/ttyUSB0'    # Check which port is being used on your controller   "/dev/ttyUSB0".encode('utf-8')
													# ex) Windows: "COM1"   Linux: "/dev/ttyUSB0" Mac: "/dev/tty.usbserial-*"
	TORQUE_ENABLE               = 1                 # Value for enabling the torque
	TORQUE_DISABLE              = 0                 # Value for disabling the torque
	P_GAIN                      = 24
	I_GAIN                      = 6

	# Position Control Parameters
	DXL_MINIMUM_POSITION_VALUE  = 10                # Dynamixel will rotate between this value
	DXL_MAXIMUM_POSITION_VALUE  = 4000              # and this value (note that the Dynamixel would not move when the position value is out of movable range. Check e-manual about the range of the Dynamixel you use.)
	DXL_MOVING_STATUS_THRESHOLD = 20                # Dynamixel moving status threshold
	index = 0
	dxl_goal_position = [DXL_MINIMUM_POSITION_VALUE, DXL_MAXIMUM_POSITION_VALUE]         # Goal position
	TARGET = 2048

	# Initialize PortHandler instance
	# Set the port path
	# Get methods and members of PortHandlerLinux or PortHandlerWindows
	portHandler = PortHandler(DEVICENAME)

	# Initialize PacketHandler instance
	# Set the protocol version
	# Get methods and members of Protocol1PacketHandler or Protocol2PacketHandler
	packetHandler = PacketHandler(PROTOCOL_VERSION)

	def __init__(self):
		
		rospy.init_node('dxl_motor', anonymous=True)
		rospy.Subscriber("/joy", Joy, self.cbGetJoy)
		rospy.Subscriber("/force_pi/control_effort", Float64 , self.cbControlEffort)
		self.pub_rec  = rospy.Publisher("/output/rec/velocity" , Float32MultiArray , queue_size=10 )
		self.pub_posi = rospy.Publisher("/current_pos" , Float64 , queue_size=10 )
		self.pub_velo = rospy.Publisher("/force_pi/state" , Float64 , queue_size=10 )
		self.output_velocity.data = [0.0]*2

		# Open port
		if self.portHandler.openPort():
			print("Succeeded to open the port")
		else:
			print("Failed to open the port")
			print("Press any key to terminate...")
			getch()
			quit()

		# Set port baudrate
		if self.portHandler.setBaudRate(self.BAUDRATE):
			print("Succeeded to change the baudrate")
		else:
			print("Failed to change the baudrate")
			print("Press any key to terminate...")
			getch()
			quit()

		# SET PI gain for Velocity Control
		self.set_P_GAIN()
		self.set_I_GAIN()
	
		# Enable Dynamixel Torque
		dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(self.portHandler, self.DXL_ID, self.ADDR_MX_TORQUE_ENABLE, self.TORQUE_ENABLE)
		if dxl_comm_result != COMM_SUCCESS:
			print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
		elif dxl_error != 0:
			print("%s" % self.packetHandler.getRxPacketError(dxl_error))
		else:
			print("Torque has been successfully enable")
	
	def set_P_GAIN(self):
		dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(self.portHandler, self.DXL_ID, self.ADDR_MX_P_GAIN, self.P_GAIN)
		# print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
		if dxl_comm_result != COMM_SUCCESS:
			print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
		elif dxl_error != 0:
			print("%s" % self.packetHandler.getRxPacketError(dxl_error))
		else:
			print("P GAIN has been successfully received")

	def set_I_GAIN(self):
		dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(self.portHandler, self.DXL_ID, self.ADDR_MX_I_GAIN, self.I_GAIN)
		# print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
		if dxl_comm_result != COMM_SUCCESS:
			print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
		elif dxl_error != 0:
			print("%s" % self.packetHandler.getRxPacketError(dxl_error))
		else:
			print("I GAIN has been successfully received")

	def cbGetJoy(self,data):
		self.data_joy = data
		if   self.data_joy.buttons[0] == 1:
			self.TARGET = 2048 #   0
		elif self.data_joy.buttons[3] == 1:
			self.TARGET = 2958 # +80
		elif self.data_joy.buttons[1] == 1:
			self.TARGET = 1900 # -12
		elif self.data_joy.buttons[2] == 1:
			self.TARGET = 3070 # +90
		# elif self.data_joy.buttons[4] == 1:
		# 	self.I_GAIN = self.I_GAIN - 1
		# 	self.set_I_GAIN()
		# elif self.data_joy.buttons[5] == 1:
		# 	self.I_GAIN = self.I_GAIN + 1
		# 	self.set_I_GAIN()
		# elif self.data_joy.buttons[6] == 1:
		# 	self.P_GAIN = self.P_GAIN - 1
		# 	self.set_P_GAIN()
		# elif self.data_joy.buttons[7] == 1:
		# 	self.P_GAIN = self.P_GAIN + 1
		# 	self.set_P_GAIN()

	def cbControlEffort(self,data):
		self.control_effort = data.data #rpm
	
	def get_position(self):
		# Read present position
		dxl_present_position, dxl_comm_result, dxl_error = self.packetHandler.read2ByteTxRx(self.portHandler, self.DXL_ID, self.ADDR_MX_PRESENT_POSITION)
		if dxl_comm_result != COMM_SUCCESS:
			print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
		elif dxl_error != 0:
			print("%s" % self.packetHandler.getRxPacketError(dxl_error))

		self.current_pos.data = (dxl_present_position * 0.088) -180
		self.pub_posi.publish(self.current_pos)
		return dxl_present_position
		# print("[ID:%03d]  PresPos:%03d" % (self.DXL_ID, dxl_present_position))
		# if not abs(dxl_goal_position[index] - dxl_present_position) > DXL_MOVING_STATUS_THRESHOLD:
		#     break
	
	def get_velocity(self):
		dxl_present_speed, dxl_comm_result, dxl_error = self.packetHandler.read2ByteTxRx(self.portHandler, self.DXL_ID, self.ADDR_MX_PRESENT_SPEED)
		if dxl_comm_result != COMM_SUCCESS:
			print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
		elif dxl_error != 0:
			print("%s" % self.packetHandler.getRxPacketError(dxl_error))

		# feedback velocity state
		if dxl_present_speed > 1023:
			self.feedback_speed.data = (dxl_present_speed - 1024) * 0.114
		else:
			self.feedback_speed.data =  dxl_present_speed * 0.114
		self.pub_velo.publish(self.feedback_speed)		 
		return dxl_present_speed
	
	def pub_speed(self, dxl_speed):
		# Write goal position
		dxl_comm_result, dxl_error = self.packetHandler.write2ByteTxRx(self.portHandler, self.DXL_ID, self.ADDR_MX_MOVING_SPEED, dxl_speed)
		if dxl_comm_result != COMM_SUCCESS:
			print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
		elif dxl_error != 0:
			print("%s" % self.packetHandler.getRxPacketError(dxl_error))
		
	def pub_datarec(self,state_,dxl_speed):
		velocity = self.get_velocity()
		if velocity > 1023:
			velocity = velocity - 1024
		if state_ == " CW ": # rpm
			self.output_velocity.data[0] = (dxl_speed - 1024) * 0.114 
			self.output_velocity.data[1] = velocity * 0.114
		else:
			self.output_velocity.data[0] = dxl_speed * 0.114 
			self.output_velocity.data[1] = velocity * 0.114
		self.pub_rec.publish(self.output_velocity)

	# def main_run(self):
	# 	state_ = "_"
	# 	dxl_speed = 0
		
	# 	while 1:
	# 		posi_ = self.get_position()
	# 		if abs(self.TARGET - posi_) < 20: # position thrashold
	# 			dxl_speed = 0
	# 			state_ = "Stop"
	# 		elif self.TARGET - posi_ > 0:
	# 			dxl_speed = 30
	# 			state_ = " CCW"
	# 		elif self.TARGET - posi_ < 0:
	# 			dxl_speed = 1024+30  # -1024 = 20 
	# 			state_ = " CW "

	# 		self.pub_speed(dxl_speed)
	# 		self.pub_datarec(state_, dxl_speed)
	# 		str_ = ("State [{}] Target {:05.2f} Postion {:05.2f} P_gain {:05.2f} I_gain {:05.2f}".format(state_,(self.TARGET*0.088)-180,(posi_*0.088)-180,self.P_GAIN/8.0,(self.I_GAIN*1000)/2048.0))
	# 		rospy.loginfo(str_)
	
	def main_run_2(self):
		state_ = "_"
		dxl_speed = 0
		
		while 1:
			posi_ = self.get_position()
			velo_ = self.get_velocity()
			velo_effort = int(round(self.control_effort / 0.144))
			# if abs(self.TARGET - posi_) < 50: # position thrashold
			# 	# get velocity from PI try to reach setpoint at 0
			# 	dxl_speed = velo_effort
			# 	state_ = "Stop"
			if self.TARGET - posi_ > 0:
				if velo_effort < 0:
					dxl_speed = 1024 + abs(velo_effort)
				else:
					dxl_speed = velo_effort
				state_ = " CCW"
			elif self.TARGET - posi_ < 0:
				if velo_effort < 0:
					dxl_speed = abs(velo_effort)
				else:
					dxl_speed = 1024 + velo_effort  # -1024 = 20 
				state_ = " CW "

			self.pub_speed(dxl_speed)
			str_ = ("State [{}] Target {:05.2f} Postion {:05.2f} Effort {:02d} Velo {}".format(state_,(self.TARGET*0.088)-180,(posi_*0.088)-180,velo_effort,velo_))
			rospy.loginfo(str_)
		 
if __name__ == '__main__':
	try:
		run_dxl = dxl_controller()	
		# run_dxl.main_run()
		run_dxl.main_run_2()
		rospy.spin()
	except rospy.ROSInterruptException:
		pass