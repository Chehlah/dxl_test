#!/usr/bin/env python
'''
	position control in CPM mode
	1 dof

	id: 12
	init: 1024
	min: 910
	max: 2048
'''

import rospy
from dynamixel_msgs.msg import JointState
from std_msgs.msg import Float64
from std_msgs.msg import Int8
from sensor_msgs.msg import Joy
from geometry_msgs.msg import WrenchStamped
from dxl_test_msg.msg import vectorForce
from dxl_test_msg.msg import rawForce
from geometry_msgs.msg import Vector3

import math as m

class cpm_mode:
	# Attributes
	data_ft = WrenchStamped()
	data_joy = Joy()
	data_F_product = vectorForce()
	force_up10 = Vector3() # type of msg force is Vector3 in geometry
	data_F_raw = rawForce()
	target = 0.0    # radius
	current_pos = 0.0
	status_moving = False
	mass_handle = 0.80
	stage = 0
	round = 0
	mass_zero = 0.0

	def __init__(self):
		rospy.init_node('cpm_mode', anonymous=True)
		self.subFT = rospy.Subscriber("/ft_sensor/netft_data", WrenchStamped , self.cbFTsensor)
		self.subJoy = rospy.Subscriber("/joy", Joy, self.cbGetJoy)
		self.subM1 = rospy.Subscriber("/joint_1_controller/state", JointState , self.cbJoinStateM1)
		self.subTarget = rospy.Subscriber("/target", Int8 , self.cbTarget)
		self.pubM1 = rospy.Publisher("/joint_1_controller/command", Float64 , queue_size=10)
		self.pubForce = rospy.Publisher("/force_raw_product", rawForce , queue_size=10)
		
	
	# Method
	# subscribe function
	def cbFTsensor(self,data):
		self.data_ft = data
		self.publish_rawForce()

	def cbGetJoy(self,data):
		self.data_joy = data
		if self.data_joy.buttons[3] == 1:
			self.target = 1.62316 #1.5708 # 93 degree
			self.publish_M1()
		elif self.data_joy.buttons[0] == 1:
			self.target = -0.0872665 # -5 degree
			self.publish_M1()
		elif self.data_joy.buttons[1] == 1:
			self.target = 0.0
			self.publish_M1()
		elif self.data_joy.buttons[2] == 1:
			self.target = 1.5708
			self.publish_M1()
		elif self.data_joy.buttons[5] == 1:
			self.mass_zero = self.data_ft.wrench.force.x
	
	def cbTarget(self,data):
		if data.data == 1:
			self.target = 1.62316 #1.5708 # 93 degree
			self.publish_M1()
		elif data.data == 2:
			self.target = -0.0872665 # -5 degree
			self.publish_M1()
		elif data.data == 3:
			self.target = 0.0
			self.publish_M1()
		elif data.data == 4:
			self.target = 1.5708
			self.publish_M1()
	
	def cbJoinStateM1(self,data):
		self.current_pos = data.current_pos
		self.status_moving = data.is_moving

	# publish function
	def publish_M1(self):
		data = Float64()
		data.data = self.target
		self.pubM1.publish(data)
	
	def publish_rawForce(self):
		x = 0.0
		y = 0.0
		m_x =  (self.mass_handle * (m.cos((m.pi/2)-self.current_pos)-1))
		if self.stage == 0:
			if self.status_moving:
				self.stage = 1
			self.data_F_raw.status_dxl = 0
		elif self.stage == 1:
			if not self.status_moving:
				self.stage = 2
			self.data_F_raw.status_dxl = 0
		elif self.stage == 2:
			if self.status_moving:
				self.stage = 3
				self.round = self.round + 1
			self.data_F_raw.status_dxl = 0
		elif self.stage == 3:
			if not self.status_moving:
				self.stage = 0

		if self.stage == 3:		
			if m.degrees(self.current_pos) > 0.0 and m.degrees(self.current_pos) < 90.0:
				self.data_F_raw.status_dxl = 1
				x = 90-m.degrees(self.current_pos)
				y = self.mass_zero * (2e-06*m.pow(x,3) - 2e-04*m.pow(x,2) - 3.9e-03*x + 1) # wrong scale 
				self.data_F_raw.degree_dxl = 90-m.degrees(self.current_pos)
				self.data_F_raw.fx_raw = self.data_ft.wrench.force.x -m_x -y
				self.data_F_raw.fy_raw = self.data_ft.wrench.force.y -(self.mass_handle * m.sin((m.pi/2)-self.current_pos))
		else:
			self.data_F_raw.degree_dxl = 90-m.degrees(self.current_pos)
			self.data_F_raw.fx_raw = self.data_ft.wrench.force.x -m_x
			self.data_F_raw.fy_raw = self.data_ft.wrench.force.y -(self.mass_handle * m.sin((m.pi/2)-self.current_pos))
		self.pubForce.publish(self.data_F_raw)
		str_ = ("Target {:2.4f} round {:2} status {} Deg {:07.4f} Fx {:09.4f} Mass {:09.4f}"
			.format(self.target,self.round,self.data_F_raw.status_dxl,self.data_F_raw.degree_dxl,self.data_F_raw.fx_raw,self.mass_zero))
		rospy.loginfo(str_)
	
def main():
	run_cpm = cpm_mode()
	rospy.spin()

if __name__ == '__main__':
	try:
		main()
	except rospy.ROSInterruptException:
		pass