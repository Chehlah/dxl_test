#!/usr/bin/env python
'''
	Position control for robot 2 Dof
'''

import rospy
from dynamixel_msgs.msg import JointState
from std_msgs.msg import Float64
from std_msgs.msg import Int8
from sensor_msgs.msg import Joy

import math as m

class inverse2dof:
	# Attributes
	getJoy = Joy()

	L1 = 0.41 		# m
	L2 = 0.345

	# get feedback from joint
	in_theta1 = 0.0
	in_theta2 = 0.0
	theta1 = 0.0          # rad
	theta2 = 0.0   
	error_M1 = 0.0
	error_M2 = 0.0   
	error_set = 0.010
	plot_set = 0.005

	# for publish joint state
	joint1 = 0.0
	joint2 = 0.0

	# command position 
	p_x = 0.345 # 0.345   # m  #start point
	p_z = 0.41 # 0.41
	p_x_a = 10.0/100       # cm
	p_z_a = 50.0/100        
	p_x_b = p_z_b = p_x_a
	p_x_c,p_z_c = p_z_a, p_x_a
	p_x_d = p_z_d = p_z_a

	# plot graph
	plot_x = 0.0
	plot_z = 0.0

	# low pass filter param
	Xold_joint1 = 0.0
	Xold_joint2 = 0.0
	lpf_case1 = 1
	lpf_case2 = 1

	# case flow will edit later or never
	i_case = 1
	# case_flow = {
	#     1 : "s_to_a",
	#     2 : "a_to_b",
	#     3 : "b_to_c",
	#     4 : "c_to_d",
	#     5 : "d_to_a"
	# }
	# case_xz = {
	#     "s_to_a" : [0],
	#     "a_to_b" : [0]      
	# }

	def __init__(self):
		rospy.init_node('inverse_2dof', anonymous=True)

		self.subM1 = rospy.Subscriber("/joint_1_controller/state", JointState , self.cbJoinStateM1)
		self.subM2 = rospy.Subscriber("/joint_2_controller/state", JointState , self.cbJoinStateM2)
		
		self.pubM1 = rospy.Publisher("/joint_1_controller/command", Float64 , queue_size=10)
		self.pubM2 = rospy.Publisher("/joint_2_controller/command", Float64 , queue_size=10)
		self.pubPx = rospy.Publisher("/fwd/Px", Float64 , queue_size=10)
		self.pubPz = rospy.Publisher("/fwd/Pz", Float64 , queue_size=10)
		self.pub_d_Px = rospy.Publisher("/delta/Px", Float64 , queue_size=10)
		self.pub_d_Pz = rospy.Publisher("/delta/Pz", Float64 , queue_size=10)
		self.pubCurM1 = rospy.Publisher("/filter/current_pos/M1", Float64 , queue_size=10)
		self.pubCurM2 = rospy.Publisher("/filter/current_pos/M2", Float64 , queue_size=10)
	
	# Method
	def publish_M1(self):
		data = Float64()
		data.data = self.joint1
		self.pubM1.publish(data)
	def publish_M2(self):
		data = Float64()
		data.data = self.joint2
		self.pubM2.publish(data)
	
	def publish_Px(self):
		data = Float64()
		data.data = self.plot_x
		self.pubPx.publish(data)
	def publish_Pz(self):
		data = Float64()
		data.data = self.plot_z
		self.pubPz.publish(data)
	def publish_delta_Px(self):
		data = Float64()
		data.data = abs(self.plot_x-self.p_x)
		self.pub_d_Px.publish(data)
	def publish_delta_Pz(self):
		data = Float64()
		data.data = abs(self.plot_z-self.p_z)
		self.pub_d_Pz.publish(data) 

	def publish_CurM1(self):
		data = Float64()
		if self.lpf_case1 == 1:
			self.Xold_joint1 = self.theta1
			self.lpf_case1 = 0
		# data_out = self.in_theta1*0.1 + self.Xold_joint1*0.9
		if  abs(self.Xold_joint1 - self.theta1) < 0.02:
			data_out = self.theta1
		else:
			data_out = self.Xold_joint1
		data.data = data_out
		self.Xold_joint1 = data_out
		self.theta1 = data_out
		self.pubCurM1.publish(data)
	def publish_CurM2(self):
		data = Float64()
		if self.lpf_case2 == 1:
			self.Xold_joint2 = self.theta2
			self.lpf_case2 = 0
		# data_out = self.in_theta2*0.1 + self.Xold_joint2*0.9
		if  abs(self.Xold_joint2 - self.theta2) < 0.02:
			data_out = self.theta2
		else:
			data_out = self.Xold_joint2
		data.data = data_out
		self.Xold_joint2 = data_out
		self.theta2 = data_out
		self.pubCurM2.publish(data)

	def cbJoinStateM1(self,data):
		# self.in_theta1 = data.current_pos
		self.theta1 = data.current_pos
		self.error_M1 = data.error
		self.publish_CurM1()
		# self.run_main()

	def cbJoinStateM2(self,data):
		# self.in_theta2 = data.current_pos
		self.theta2 = data.current_pos
		self.error_M2 = data.error
		self.publish_CurM2()
		self.run_main() # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

	def forwardKinematic(self):
		self.plot_z = (self.L1*m.cos(self.theta1))+(self.L2*m.cos(self.theta1+self.theta2))
		self.plot_x = (self.L1*m.sin(self.theta1))+(self.L2*m.sin(self.theta1+self.theta2))
			
	def inverseKinematic(self):
		self.joint2 = m.acos((self.p_x*self.p_x+self.p_z*self.p_z-self.L1*self.L1-self.L2*self.L2)/(2*self.L1*self.L2))
		self.joint1 = m.atan2(self.p_x,self.p_z)-m.acos((self.p_x*self.p_x+self.p_z*self.p_z+self.L1*self.L1-self.L2*self.L2)/(2*self.L1*m.sqrt(self.p_x*self.p_x+self.p_z*self.p_z)))
		
	def controlPart_rectangular(self):
		# setpoint     : p_x,p_z 
		# feedback  : plot_x,plot_z
		# self.publish_delta_Px()
		# self.publish_delta_Pz()

		if self.i_case == 1:
			self.p_x,self.p_z = self.p_x_a,self.p_z_a
			if abs(self.plot_x-self.p_x_a) <= self.plot_set and abs(self.plot_z-self.p_z_a) <= self.plot_set:
				self.i_case = 2
		elif self.i_case == 2:
			if abs(self.plot_x-self.p_x) <= self.plot_set and abs(self.plot_z-self.p_z) <= self.plot_set:
				if abs(self.plot_x-self.p_x_b) <= self.plot_set and abs(self.plot_z-self.p_z_b) <= self.plot_set:
					self.i_case = 3
				else:
					self.p_x,self.p_z = self.p_x,self.p_z-(1.0/100)
		elif self.i_case == 3:
			if abs(self.plot_x-self.p_x) <= self.plot_set and abs(self.plot_z-self.p_z) <= self.plot_set:
				if abs(self.plot_x-self.p_x_c) <= self.plot_set and abs(self.plot_z-self.p_z_c) <= self.plot_set:
					self.i_case = 4
				else:
					self.p_x,self.p_z = self.p_x+(1.0/100),self.p_z
		elif self.i_case == 4:
			if abs(self.plot_x-self.p_x) <= self.plot_set and abs(self.plot_z-self.p_z) <= self.plot_set:
				if abs(self.plot_x-self.p_x_d) <= self.plot_set and abs(self.plot_z-self.p_z_d) <= self.plot_set:
					self.i_case = 5
				else:
					self.p_x,self.p_z = self.p_x,self.p_z+(1.0/100)
		elif self.i_case == 5:
			if abs(self.plot_x-self.p_x) <= self.plot_set and abs(self.plot_z-self.p_z) <= self.plot_set:
				if abs(self.plot_x-self.p_x_a) <= self.plot_set and abs(self.plot_z-self.p_z_a) <= self.plot_set:
					self.i_case = 6
				else:
					self.p_x,self.p_z = self.p_x-(1.0/100),self.p_z


	def run_main(self):
		# self.inverseKinematic()
		self.forwardKinematic()
		# self.controlPart_rectangular()

		str_=("case{} dP_x,z{:.3f},{:.3f} P_x,z{:.3f},{:.3f} plot>x,z{:.3f},{:.3f} J1,J2{:.3f},{:.3f} fb{:.3f},{:.3f}"
			.format(self.i_case,abs(self.p_x-self.plot_x),abs(self.p_z-self.plot_z),self.p_x,self.p_z,self.plot_x,self.plot_z,self.joint1,self.joint2,self.theta1,self.theta2))
		rospy.loginfo(str_)

		# self.publish_M1()
		# self.publish_M2()
		self.publish_Px()
		self.publish_Pz()

def main():
	run_inverse = inverse2dof()
	rospy.spin()

if __name__ == '__main__':
	try:
		main()
	except rospy.ROSInterruptException:
		pass