#!/usr/bin/env python
'''
	Position and maybe Force-Velocity control in CPM mode.
	Control a robot for recording raw data samples of EMG, Theta and Force.
	setup Dynamixel in wheel mode.

	readme file name "step emg cpm"

	start: 31/01/2020
'''

import rospy
from std_msgs.msg import Float64MultiArray
from dynamixel_msgs.msg import JointState
from std_msgs.msg import Float64
from sensor_msgs.msg import Joy
from dxl_test_msg.msg import dataTrain
from geometry_msgs.msg import WrenchStamped
import math as m 

class cpm_emg:
	data_joy = Joy()
	target = 0.0 #radius
	dxl_speed = 0.40 # rad/s
	current_pos = 0.0
	data_speed = Float64()

	status_moving = False
	round = 0
	round_check = 0
	
	data_train = dataTrain()
	mass_zero = 8 # edit N
	mass_handle = 0.8
	str_ = ""

	force_x = Float64()

	def __init__(self):
		rospy.init_node('cpm_emg_rec', anonymous=True)
		rospy.Subscriber("/myo_rms/rms_list", Float64MultiArray , self.cbEMGrms)
		rospy.Subscriber("/myo_rms/mva_list", Float64MultiArray , self.cbEMGmva)
		rospy.Subscriber("/joy", Joy, self.cbGetJoy)
		rospy.Subscriber("/joint_1_controller/state", JointState , self.cbJoinStateM1)
		# rospy.Subscriber("/ft_sensor/netft_data", WrenchStamped , self.cbFTsensor)
		rospy.Subscriber("/Fx_ma", Float64 , self.cbForceMA)
		self.pubM1 = rospy.Publisher("/joint_1_controller/command", Float64 , queue_size=10)
		self.pub_data_train = rospy.Publisher("/data_train", dataTrain , queue_size=10)

		self.str_ = "start"
		rospy.loginfo(self.str_)
		
	def cbEMGrms(self,data):
		self.data_train.rms_emg_1 = data.data[0]
		self.data_train.rms_emg_2 = data.data[1]
		self.data_train.rms_emg_3 = data.data[2]
		self.data_train.rms_emg_4 = data.data[3]
		self.data_train.rms_emg_5 = data.data[4]
		self.data_train.rms_emg_6 = data.data[5]
		self.data_train.rms_emg_7 = data.data[6]
		self.data_train.rms_emg_8 = data.data[7]

		str_ = ("Target {:2.4f} round {:2} status {} Deg {:07.4f} Fx {:09.4f}"
			.format(self.target,self.round,self.data_train.status_dxl,self.data_train.degree_dxl,self.data_train.fx_raw))
		rospy.loginfo(str_)
	
	def cbEMGmva(self,data):
		self.data_train.mva_emg_1 = data.data[0]
		self.data_train.mva_emg_2 = data.data[1]
		self.data_train.mva_emg_3 = data.data[2]
		self.data_train.mva_emg_4 = data.data[3]
		self.data_train.mva_emg_5 = data.data[4]
		self.data_train.mva_emg_6 = data.data[5]
		self.data_train.mva_emg_7 = data.data[6]
		self.data_train.mva_emg_8 = data.data[7]

	def cbJoinStateM1(self,data):
		self.current_pos = data.current_pos
		self.data_train.degree_dxl = 90-m.degrees(self.current_pos)
		# stop at target 
		if abs(self.target-self.current_pos) <= 0.01: 
			self.status_moving = False
			self.pubspeed_M1(0.0)

	def cbGetJoy(self,data):
		self.data_joy = data
		# if self.data_joy.buttons[0] == 1:
		# 	self.target = 1.62316 #1.5708 # 93 degree
		# 	self.publish_M1()
		if self.data_joy.buttons[1] == 1:
			self.target = -0.0872665 # -5 degree
			# self.round = self.round + 1
			self.publish_M1()
		# elif self.data_joy.buttons[3] == 1:
		# 	self.target = 0.0
		# 	self.publish_M1()
		elif self.data_joy.buttons[2] == 1:
			self.target = 1.5708
			self.publish_M1()
		elif self.data_joy.buttons[4] == 1:
			self.pubspeed_M1(0.0)

	def cbFTsensor(self,data):
		if self.status_moving:		
			if m.degrees(self.current_pos) >= 0.0 and m.degrees(self.current_pos) <= 91.0:
				self.data_train.status_dxl = 1
				self.data_train.fx_raw = data.wrench.force.x - (self.mass_handle * (m.cos((m.pi/2)-self.current_pos)-1))
			else:
				self.data_train.status_dxl = 0
				self.data_train.fx_raw = 0
		else:
			self.data_train.status_dxl = 0
			self.data_train.fx_raw = 0
		if self.data_train.status_dxl == 1:
			self.pub_data_train.publish(self.data_train)
		
		# # just EMG - no sub force
		# if self.status_moving:		
		# 	if m.degrees(self.current_pos) >= 0.0 and m.degrees(self.current_pos) <= 91.0:
		# 		self.data_train.status_dxl = 1
		# 		# x = 90-m.degrees(self.current_pos)
		# 		# self.data_train.fx_raw = self.mass_zero * (2e-06*m.pow(x,3) - 2e-04*m.pow(x,2) - 3.9e-03*x + 1) # wrong scale 
		# 		self.data_train.fx_raw = self.force_x.data - 4
		# 	else:
		# 		self.data_train.status_dxl = 0
		# 		self.data_train.fx_raw = 0
		# else:
		# 	self.data_train.status_dxl = 0
		# 	self.data_train.fx_raw = 0
		# if self.data_train.status_dxl == 1:
		# 	self.pub_emg_train.publish(self.data_train)
	
	def cbForceMA(self,data):
		if self.status_moving:		
			if m.degrees(self.current_pos) >= 0.0 and m.degrees(self.current_pos) <= 91.0:
				self.data_train.status_dxl = 1
				self.data_train.fx_raw = data.data - 4 - (self.mass_handle * (m.cos((m.pi/2)-self.current_pos)-1))
				self.pub_data_train.publish(self.data_train)
			else:
				self.data_train.status_dxl = 0
				self.data_train.fx_raw = 0
			self.round_check = 1
		else:
			self.data_train.status_dxl = 0
			self.data_train.fx_raw = 0
			if self.round_check == 1:
				self.round = self.round + 1
				self.round_check = 0
	
	# publish function
	def pubspeed_M1(self,data):
		self.data_speed.data = data
		self.pubM1.publish(self.data_speed)
	
	def publish_M1(self):
		# check CW by target - current position 
		if self.target-self.current_pos > 0:
			self.pubspeed_M1(self.dxl_speed)
			self.status_moving = False
		elif self.target-self.current_pos < 0:
			self.pubspeed_M1(-self.dxl_speed)
			self.status_moving = True
			# self.round = self.round + 1

if __name__ == "__main__":
	try:
		run = cpm_emg()
		rospy.spin()
	except rospy.ROSInterruptException:
		pass