#!/usr/bin/env python
'''
	Position and Force-Velocity control in CPM mode
	
	setup Dynamixel in wheel mode

	readme file name "step run cpm wheel"

'''

import rospy
from dynamixel_msgs.msg import JointState
from std_msgs.msg import Float64
from std_msgs.msg import Int8
from sensor_msgs.msg import Joy
from geometry_msgs.msg import WrenchStamped
from dxl_test_msg.msg import vectorForce
from dxl_test_msg.msg import rawForce
from geometry_msgs.msg import Vector3
from std_msgs.msg import Bool

import math as m

class cpm_wheel_pid:
	# Attributes
	data_ft = Float64()
	data_joy = Joy()
	data_F_product = vectorForce()
	force_up10 = Vector3() # type of msg force is Vector3 in geometry
	data_F_raw = rawForce()
	
	target = 0.0    # radius
	current_pos = 0.0
	status_moving = False
	mass_handle = 0.46 # mass for gravity compensate
	stage = 0
	round = 0
	mass_zero = 0.0
	dxl_speed = 0.1 # rad/s 
	data_speed = Float64()

	dfx = Float64() # feedback /joint1/state for PID controller  
	pid_en = Bool()

	n_fric = 0.5 
	fric_zero = 0.0

	def __init__(self):
		rospy.init_node('cpm_wheel_pid', anonymous=True)
		self.subFT = rospy.Subscriber("/Fx_ma", Float64 , self.cbFTsensor)
		self.subJoy = rospy.Subscriber("/joy", Joy, self.cbGetJoy)
		self.subM1 = rospy.Subscriber("/joint_1_controller/state", JointState , self.cbJoinStateM1)

		# self.pubM1 = rospy.Publisher("/joint_1_controller/command", Float64 , queue_size=10)
		self.pubForce = rospy.Publisher("/force_raw_product", rawForce , queue_size=10)
		self.pubJoint1_State = rospy.Publisher("/joint1/state", Float64 , queue_size=10)
		self.pubOmega = rospy.Publisher("/omega/const", Float64 , queue_size=10)
		self.pubM1 = rospy.Publisher("/joint_1_controller/command", Float64 , queue_size=10)
		self.pubPID_eneble = rospy.Publisher("/joint1/pid_enable", Bool , queue_size=10)
		self.pid_en.data = True
	
	# Method
	# subscribe function
	def cbFTsensor(self,data):
		self.data_ft = data
		self.publish_rawForce()
	
	def cbGetJoy(self,data):
		self.data_joy = data
		if self.data_joy.buttons[0] == 1:
			self.target = m.radians(80) #1.5708 # 93 degree
			self.publish_M1()
		elif self.data_joy.buttons[1] == 1:
			self.target = -0.0872665 # -5 degree
			self.publish_M1()
		elif self.data_joy.buttons[3] == 1:
			self.target = 0.0
			self.publish_M1()
		elif self.data_joy.buttons[2] == 1:
			self.target = 1.5708
			self.publish_M1()
		elif self.data_joy.buttons[10] == 1:
			self.target = 0.7853
			self.publish_M1()
		# elif self.data_joy.buttons[4] == 1:
		# 	self.pubOmega_M1(0.0)
		# 	if self.pid_en.data:
		# 		self.pid_en.data = False
		# 	else:
		# 		self.pid_en.data = True
		# 	self.pubPID_eneble.publish(self.pid_en)
		elif self.data_joy.buttons[5] == 1:
			if self.mass_zero == 1.0:
				self.mass_zero = 0.0
			else:
				self.mass_zero = 1.0
		elif self.data_joy.buttons[4] == 1:
			if self.fric_zero == 1.0:
				self.fric_zero = 0.0
			else:
				self.fric_zero = 1.0
	
	def cbJoinStateM1(self,data):
		self.current_pos = data.current_pos
		# stop at target 
		if abs(self.target-self.current_pos) <= 0.01: 
			self.status_moving = False
			self.pubOmega_M1(0.0)
		if m.degrees(self.current_pos) < -1.0 and m.degrees(self.current_pos) > 91.5:
			self.status_moving = False
			self.pubOmega_M1(0.0)

	# publish function
	# def pubspeed_M1(self,data):
	# 	self.data_speed.data = data
	# 	self.pubM1.publish(self.data_speed)
	
	def pubOmega_M1(self,data):
		self.data_speed.data = data
		self.pubOmega.publish(self.data_speed)
		self.pubM1.publish(self.data_speed)
		

	def publish_M1(self):
		# check CW by target - current position 
		if self.target-self.current_pos > 0:
			self.pubOmega_M1(self.dxl_speed)
			self.status_moving = False
		elif self.target-self.current_pos < 0:
			self.pubOmega_M1(-self.dxl_speed)
			self.status_moving = True
			self.round = self.round + 1
	
	def publish_rawForce(self):
		x = 0.0
		y = 0.0
		m_x =  (self.mass_handle * (m.cos((m.pi/2)-self.current_pos)-1)) * self.mass_zero
		if self.status_moving:		
			if m.degrees(self.current_pos) > 0.0 and m.degrees(self.current_pos) < 90.0:
				self.data_F_raw.status_dxl = 1
				x = 90-m.degrees(self.current_pos)
				# y = self.mass_zero * (2e-06*m.pow(x,3) - 2e-04*m.pow(x,2) - 3.9e-03*x + 1) # wrong scale 
				self.data_F_raw.degree_dxl = 90-m.degrees(self.current_pos)
				self.data_F_raw.fx_mav = self.data_ft.data -4 - m_x - (self.n_fric * self.fric_zero) # -y
				# self.data_F_raw.fy_raw = self.data_ft.wrench.force.y -(self.mass_handle * m.sin((m.pi/2)-self.current_pos))
				# self.force_velocity_control()
				self.dfx.data = -(self.data_ft.data -m_x)
		else:
			self.data_F_raw.status_dxl = 0
			self.data_F_raw.degree_dxl = 90-m.degrees(self.current_pos)
			self.data_F_raw.fx_mav = self.data_ft.data - m_x -4
			# self.data_F_raw.fy_raw = self.data_ft.wrench.force.y -(self.mass_handle * m.sin((m.pi/2)-self.current_pos))
			self.dfx.data = 0.0
		self.pubForce.publish(self.data_F_raw)
		self.pubJoint1_State.publish(self.dfx)

		str_ = ("Target {:2.4f} round {:2} status {} Deg {:07.4f} Fx {:09.4f} Mass0 {:03.1f} Fric0 {:03.1f}"
			.format(self.target,self.round,self.data_F_raw.status_dxl,self.data_F_raw.degree_dxl,self.data_F_raw.fx_mav,self.mass_zero,self.fric_zero))
		rospy.loginfo(str_)
	
	# logical function
	def force_velocity_control(self):
		dfx = self.data_F_raw.fx_mav
		dxl_speed = self.dxl_speed
		if dfx >= 1.0 and dfx <= 2.0:
			dxl_speed = -(dxl_speed - (0.4 * (dfx-1.0)))
		elif dfx > 2.0 and dfx <= 3.0:
			dxl_speed = 0.0
		elif dfx > 3.0 and dfx <= 5.0:
			dxl_speed = 0.25 * (dfx-3.0)  
		elif dfx > 5.0:
			self.target = 1.5708
			self.publish_M1()
		else:
			dxl_speed = -dxl_speed
		self.pubOmega_M1(dxl_speed)
			
	
def main():
	run_cpm = cpm_wheel_pid()
	rospy.spin()

if __name__ == '__main__':
	try:
		main()
	except rospy.ROSInterruptException:
		pass