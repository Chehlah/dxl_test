#!/usr/bin/env python
'''
	Velocity control at end-effector

	set dynamixel in wheel mode
	!!! be carefull !!!
	
	re-Write in OOP

	joint setup 
	>> min 1024
	>> max 3072 
'''
# 'Droid Sans Mono', 'monospace', monospace, 'Droid Sans Fallback'

import rospy
from dynamixel_msgs.msg import JointState
from std_msgs.msg import Float64
from std_msgs.msg import Int8
from sensor_msgs.msg import Joy

import math as m

class jacobian2dof:
	# Attributes
	jM1 = 0.0015
	jM2 = 0.0015
	omega1 = 0.0
	omega2 = 0.0

	directionX = 0

	L1 = 0.41 		# mm
	L2 = 0.345

	Vconst = 0.05	# 0.05
	Vx = 0.0 		# m/s
	Vz = 0.0

	getJoy = Joy()
	data1 = Float64()
	data2 = Float64()

	def __init__(self):
		rospy.init_node('jacobian_class', anonymous=True)
		
		self.subM1 = rospy.Subscriber("/pan_1_controller/state", JointState , self.cbJoinStateM1)
		self.subM2 = rospy.Subscriber("/pan_2_controller/state", JointState , self.cbJoinStateM2)
		self.subDirec = rospy.Subscriber("/directionX", Int8, self.cbDirectionX)
		self.subJoy = rospy.Subscriber("/joy", Joy, self.cbGetJoy)

		self.pubM1speed = rospy.Publisher("/pan_1_controller/command", Float64 , queue_size=10)
		self.pubM2speed = rospy.Publisher("/pan_2_controller/command", Float64 , queue_size=10)

	# Method
	def publishSpeed(self):
		self.data1.data = self.omega1
		self.data2.data = self.omega2
		self.pubM1speed.publish(self.data1)
		self.pubM2speed.publish(self.data2)

	def inverseJacobian(self):
		if self.jM2 >= 0.0015:
			self.omega1 = (((m.cos(self.jM1+self.jM2)*self.Vz)+(m.sin(self.jM1+self.jM2)*self.Vx))
				/(m.sin(self.jM2)*self.L1))
			self.omega2 = ((((self.L1*m.cos(self.jM1)+self.L2*m.cos(self.jM1+self.jM2))*self.Vz)
				+(self.Vx*(self.L1*m.sin(self.jM1)+self.L2*m.sin(self.jM1+self.jM2))))
				/(-(self.L1*self.L2*m.sin(self.jM2))))	
		else:
			self.omega1 = 0.0
			self.omega2 = 0.0
		
		# for safety overshoot   	
		if self.omega1 >= 1.0:
    		self.omega1 = 1.0

	def directionControl(self):
		if self.getJoy.axes[6] == 1.0 and self.getJoy.axes[7] == 0.0:
			self.Vx = self.Vconst
		elif self.getJoy.axes[6] == -1.0 and self.getJoy.axes[7] == 0.0:
			self.Vx = -self.Vconst
		elif self.getJoy.axes[6] == 0.0 and self.getJoy.axes[7] == 1.0:
			self.Vz = self.Vconst
		elif self.getJoy.axes[6] == 0.0 and self.getJoy.axes[7] == -1.0:
			self.Vz = -self.Vconst
		else:
			self.Vx = 0.0
			self.Vz = 0.0

	def cbDirectionX(self,data):
		self.directionX = data.data
		if self.directionX == 1:
			self.Vx = self.Vconst
		elif self.directionX == -1:
			self.Vx = -self.Vconst
		else:
			self.directionX = 0
			self.Vx = 0.0

	def cbGetJoy(self,data):
		self.getJoy = data
		# str_ = ("get joy {} and {}".format(self.getJoy.axes[6],self.getJoy.axes[7]))
		# rospy.loginfo(str_)
		str_ = ("Post_1:{: 3.3f} Post_2:{: 3.3f} OmegaM1:{: 3.3f} OmegaM2:{: 3.3f}"
			.format(self.jM1,self.jM2,self.omega1,self.omega2))		
		rospy.loginfo(str_)

	def cbJoinStateM1(self,data):
		self.jM1 = data.current_pos
	
	def cbJoinStateM2(self,data):
		self.jM2 = data.current_pos
		self.main_run()

	# or run while interrupt?
	def main_run(self):
		self.directionControl()
		self.inverseJacobian()
		# str_ = ("Post_1:{: 3.3f} Post_2:{: 3.3f} OmegaM1:{: 3.3f} OmegaM2:{: 3.3f}"
		# 	.format(self.jM1,self.jM2,self.omega1,self.omega2))		
		# rospy.loginfo(str_)
		self.publishSpeed()

# for init topic
def main():
	jc = jacobian2dof()
	# loop here with some rate?
	rospy.spin()

if __name__ == '__main__':
	try:
		main()
	except rospy.ROSInterruptException:
		pass