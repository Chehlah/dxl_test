# connect port
sudo chmod a+rw /dev/input/js0 
>> set joy Logitech to "Xinput"
rosrun joy joy_node

# launch Dynamixel
sudo chmod a+rw /dev/ttyUSB0
roslaunch dxl_test control_manager.launch
roslaunch dxl_test start_wheel_controller.launch 
>> type: JointTorqueController

# run node 
rosrun dxl_test jacobian_2dof.py
rosrun dxl_test inverse_2dof.py
# or 
rosrun dxl_test vector_control.py
# or
rosrun dxl_test feature_1to5.py
# new
rosrun dxl_test vector_2dof.py

# launch F/T sensor w/ net_box 
>> check IP address connected on browser 
roslaunch netft_rdt_driver ft_sensor.launch 
>> check setting param in launch file 
rosservice call /ft_sensor/bias_cmd "cmd: 'bias'"

# TF transform for RVIZ
rosrun tf static_transform_publisher 0.0 0.0 0.0 0.0 0.0 0.0 map eff 100

# topic
rostopic echo ..

rostopic pub -1 /target std_msgs/Int8 "data: 1"
rostopic echo /topic_name -p > file_name.csv

# service 
rosservice set slope >> 1.0 ?

rostopic echo /topic_name -p > ตามด้วยชื่อไฟล์.csv

# Topic for 1 to 5 
// position in degree (not ready)
rostopic pub -1 /target std_msgs/Float64 "data: 0.0"
// speed in radian/sec
rostopic pub -1 /wheel_1_controller/command std_msgs/Float64 "data: 0.0"