# run joy
sudo chmod a+rw /dev/input/js0 
>> set joy Logitech to "Xinput"
rosrun joy joy_node
rostopic echo /joy

# launch dynamixel wheel mode ID:12
sudo chmod a+rw /dev/ttyUSB0
roslaunch dxl_test control_manager.launch
roslaunch dxl_test start_cpm_wheel_mode.launch

# launch F/T sensor w/ net_box 
>> check IP address connected on browser 
>> check setting param in launch file 
roslaunch netft_rdt_driver ft_sensor.launch 
Note : move link to position 90 degrees and Launch force_pkg that on bias  

# run service 'bias' F/T sensor
No: rosservice call /ft_sensor/bias_cmd "cmd: 'bias'"

# run force mva
rosrun dxl_test force_ma.py

rqt_graph
-------------------------------------------------------------------------------

# run main
No: rosrun dxl_test cpm_wheel_pid.py

rosrun dxl_test cpm_wheel_pid_tune.py
rqt_plot

# run v_controller
rosrun dxl_test v_controller.py

# launch PID controller
roslaunch dxl_test pid_controller.launch

rostopic echo /topic_name -p > file_name.csv
-------------------------------------------------------------------------------

# Step run PI controller for DXL w/ SDK

# run joy
sudo chmod a+rw /dev/input/js0 
>> set joy Logitech to "Xinput"
rosrun joy joy_node
rostopic echo /joy

# dxl 
sudo chmod a+rw /dev/ttyUSB0
rosrun dxl_test dxl_manual_control.py

rostopic echo /output/rec/velocity -p > pi_tune_1.csv


>>> def cal_pi(p,i):
...     p = p/8 = 28/8 = 3.5
...     i = i*2048/1000 = 12.28
...     print (p)
...     print (i)

-------------------------------------------------------------------------------
# Force-Velocity PI Controller
# 1
roscore
# 2
rqt_multiplot
# 3
sudo chmod a+rw /dev/input/js0
rosrun joy joy_node
# 5
rostopic echo /joy
# 4
rosrun dxl_test force_pi_2.py
# 6
sudo chmod a+rw /dev/ttyUSB0
rosrun dxl_test dxl_manual_control_2.py
# 7
roslaunch netft_rdt_driver ft_sensor.launch 
# 8
roslaunch dxl_test pid_controller.launch

# New window
# 1
sudo chmod a+rw /dev/ttyACM0
roslaunch ros_myo myo.launch
# 2
roscd dxl_test/script
python force_compensation.py 
# 3 
rosrun dxl_test emg_rec_raw.py
rostopic echo /rec_raw/emg -p > emg_rec_raw_noforce.csv
# 4
rostopic echo /output/rec/force_velocity -p > rec_f_20_01.csv
