# code experimental for control 2dof robot arm

1. inverse_2dof.py
run position control 
>> step_run.md

2. jacobian_2dof.py
run velocity control
>> step_run_velocity.md

3. cpm_mode.py
run continuous passive motion with gravity compensation
>> step_run_cpm.md