# ROS master
1. roscore

# launch dynamixel wheel mode
sudo chmod a+rw /dev/ttyUSB0
2. roslaunch dxl_test control_manager.launch
3. roslaunch dxl_test start_robot_4w.launch

# FT sensor
5. roslaunch netft_rdt_driver ft_sensor.launch
>> /ft_sensor/netft_data/wrench/force/
6. rosrun dxl_test force_4w_ma.py
rosservice call /ft_sensor/bias_cmd "cmd: 'bias'"

# Run 
7. rosrun dxl_test robot_4w_control.py
# adjusting the friction and respond
8. rqt 

rostopic echo /pub/position
rqt_multiplot

# Joy
sudo chmod a+rw /dev/input/js0 
>> set joy Logitech to "Xinput"
4. rosrun joy joy_node
rostopic echo /joy

