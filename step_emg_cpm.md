Note: git remote set-url origin https:

# ROS master
1. roscore

# launch dynamixel wheel mode
sudo chmod a+rw /dev/ttyUSB0
2. roslaunch dxl_test control_manager.launch
3. roslaunch dxl_test start_cpm_wheel_mode.launch

# Force
# launch F/T sensor w/ net_box 
>> check IP address connected on browser 
4. roslaunch netft_rdt_driver ft_sensor.launch 
>> check setting param in launch file 
# run service 'bias' F/T sensor
3. rosservice call /ft_sensor/bias_cmd "cmd: 'bias'"
# Topic echo Force-x
rostopic echo /ft_sensor/netft_data/wrench/force/x
 
# Joy
sudo chmod a+rw /dev/input/js0 
>> set joy Logitech to "Xinput"
5. rosrun joy joy_node
rostopic echo /joy

6. rqt_graph
-------------------------------------------------------------------------

## New windows
# CPM wheel mode
1. rosrun dxl_test cpm_emg_rec.py
1. rosrun dxl_test cpm_emg_rec_6.py
1. rosrun dxl_test cpm_emg_rec_6_2.py
1. rosrun dxl_test cpm_emg_rec_raw.py

# Force Moving Average
2. rosrun dxl_test force_ma.py

# Sample node (add to launch)
3. rosrun ros_myo myo_rms.py 
3. rosrun ros_myo myo_rms_mva_6.py
3. rosrun ros_myo myo_rms_mva_6_2.py

# Myo 
sudo chmod a+rw /dev/ttyACM0
4. roslaunch ros_myo myo.launch

# Record topic 
rostopic echo /topic_name -p > file_name.csv
rostopic echo /data_raw -p > emg_rec_raw_aufa.csv

------------------------------------------------------------------------
# Note: 11/04/2023
# Quick Lab 

roscore

# launch dynamixel wheel mode
sudo chmod a+rw /dev/ttyUSB0
roslaunch dxl_test control_manager.launch
roslaunch dxl_test start_cpm_wheel_mode.launch
rostopic hz /joint_1_controller/state 

# Joy
sudo chmod a+rw /dev/input/js0 
rosrun joy joy_node

# Run main
rosrun dxl_test cpm_emg_rec_raw.py

# Force
# launch F/T sensor w/ net_box 
roslaunch netft_rdt_driver ft_sensor.launch 
rosrun dxl_test force_ma.py

# Myo 
sudo chmod a+rw /dev/ttyACM0
roslaunch ros_myo myo.launch

rostopic echo /data_raw -p > emg_cpm_up_01.csv
rostopic echo /data_raw -p > emg_cpm_dw_01.csv
